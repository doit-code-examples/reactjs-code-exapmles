import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter as Router, Route, Switch, withRouter } from 'react-router-dom'
import Company from './components/Company'
import ContentAttributes from './components/Company/ContentAttributes'
import Edit from './components/Company/Edit'
import Content from './components/Content'
import ContentForm from './components/Content/Form'
import DashboardPage from './components/Dashboard'
import ForgotPassword from './components/ForgotPassword'
import Login from './components/Login'
import Marketers from './components/Marketers'
import AddMarketer from './components/Marketers/AddMarketer'
import MarketerDetails from './components/Marketers/Details'
import NotFound from './components/NotFound'
import Salespersons from './components/Salespersons'
import AddSalesperson from './components/Salespersons/AddSalesperson'
import SalespersonDetails from './components/Salespersons/Details'
import Settings from './components/Settings'
import Blank from './layouts/Blank'
import Dashboard from './layouts/Dashboard'

const DashboardLayout = withRouter(
  connect(state => {
    const { auth, layout, company } = state
    return {
      auth,
      layout,
      company
    }
  })(Dashboard)
)

const EmptyLayout = withRouter(
  connect(state => {
    const { auth } = state
    return {
      auth
    }
  })(Blank)
)

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route {...rest} render={props => (
    <Layout>
      <Component {...props} />
    </Layout>
  )} />
)

class Routes extends Component {
  render() {
    return (
      <Switch>
        <AppRoute
          path="/"
          exact
          layout={DashboardLayout}
          component={withRouter(DashboardPage)}
        />
        <AppRoute path="/content" exact layout={DashboardLayout} component={withRouter(Content)} />
        <AppRoute path="/content/add" exact layout={DashboardLayout} component={withRouter(ContentForm)} />
        <AppRoute path="/content/:cardId/edit" exact layout={DashboardLayout} component={withRouter(ContentForm)} />
        <AppRoute
          path="/marketers"
          exact
          layout={DashboardLayout}
          component={withRouter(Marketers)}
        />
        <AppRoute
          path="/marketers/add"
          exact
          layout={DashboardLayout}
          component={withRouter(AddMarketer)}
        />
        <AppRoute
          path="/marketers/:marketerId"
          exact
          layout={DashboardLayout}
          component={withRouter(MarketerDetails)}
        />
        <AppRoute
          path="/salespersons"
          exact
          layout={DashboardLayout}
          component={withRouter(Salespersons)}
        />
        <AppRoute
          path="/salespersons/add"
          exact
          layout={DashboardLayout}
          component={withRouter(AddSalesperson)}
        />
        <AppRoute
          path="/salespersons/:salespersonId"
          exact
          layout={DashboardLayout}
          component={withRouter(SalespersonDetails)}
        />
        <AppRoute
          path="/company"
          exact
          layout={DashboardLayout}
          component={withRouter(Company)}
        />
        <AppRoute
          path="/company/edit"
          exact
          layout={DashboardLayout}
          component={withRouter(Edit)}
        />
        <AppRoute
          path="/settings"
          exact
          layout={DashboardLayout}
          component={withRouter(Settings)}
        />
        <AppRoute path="/company/attributes" exact layout={DashboardLayout} component={withRouter(ContentAttributes)} />

        <AppRoute path="/login" exact layout={EmptyLayout} component={connect(state => ({ auth: state.auth }))(Login)}/>
        <AppRoute path="/forgot-password" exact layout={EmptyLayout} component={ForgotPassword} />
        <AppRoute layout={EmptyLayout} component={withRouter(NotFound)} />
      </Switch>
    )
  }
}


const RoutesWrapper = withRouter(
  connect(state => {
    const { auth } = state
    return {
      auth
    }
  })(Routes)
)

export default function App() {
  return <Router><RoutesWrapper/></Router>
}
