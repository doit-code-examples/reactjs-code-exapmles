import RequestsManager from './RequestsManager'

const manager = new RequestsManager()
export function request(...args) {
  return new Promise((resolve, reject) => {
    manager.push(args, resolve, reject)
  })
}
