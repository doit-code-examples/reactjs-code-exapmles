import request from './request'

export default class RequestsManager {
  attempts = 0

  isBusy = false

  queue = []

  push(args, resolve, reject) {
    if (this.isBusy) {
      this.queue.push({ args, resolve, reject });
    } else {
      return this.execute({ args, resolve, reject });
    }
  }

  execute(data) {
    this.isBusy = true
    return request(...data.args)
      .then((r) => {
        this.isBusy = false
        data.resolve(r)
        this.attempts = 0
      })
      .catch(e => {
        if (e.status === 401
          && e.message == 'jwt expired'
          && localStorage.getItem('refreshToken')
          && this.attempts < 1
        ) {
          this.queue.unshift({ args: ['/auth/refresh', 'POST', {
            refreshToken: localStorage.getItem('refreshToken')
          }], resolve: ({ token, refreshToken }) => {
            localStorage.setItem('token', token)
            localStorage.setItem('refreshToken', refreshToken)
            this.isBusy = false
            this.queue.unshift(data)
          }, reject: (error) => {
            localStorage.removeItem('token')
            localStorage.removeItem('refreshToken')
            this.isBusy = false
            data.reject(e)
            window.location.href = '/login'
          }})
          this.attempts++
        } else {
          data.reject(e)
          this.isBusy = false
        }
      })
      .finally(() => {
        if (this.queue.length > 0) {
          this.execute(this.queue.shift())
        }
      })
  }
}
