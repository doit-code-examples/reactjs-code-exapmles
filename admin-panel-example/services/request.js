import fetch from 'isomorphic-fetch';
import NetworkError from '../errors/NetworkError';
import { stringifyQuery } from '../utils/location';

const API_HOST = window.APP_STATE.company.host
  ? 'https://' + window.APP_STATE.company.host
  : ''
const PATH_PREFIX = '/api'

export function getAPIURL(endpoint) {
  return API_HOST + PATH_PREFIX + endpoint
}

export default function request(endpoint, method = 'GET', data = {}, isJSON = true) {
  return new Promise(function(resolve, reject) {
    const options = {
      url: getAPIURL(endpoint),
      method,
      headers: {
        Accept: 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
      },
      timeout: 30 * 1000
    }

    if (typeof localStorage != 'undefined' && localStorage.getItem('token')) {
      options.headers['Authorization'] =
        'Bearer ' + localStorage.getItem('token')
    }
    Object.keys(data).forEach((key) => {
      if (data[key] === undefined || (method === 'GET' && data[key] === null)) {
        delete data[key]
      }
    })

    if (isJSON) {
      options.headers['Content-Type'] = 'application/json; charset=utf-8'
      options.body = JSON.stringify(data)
    } else {
      options.body = data
    }

    if (method === 'GET') {
      if (data) {
        options.url += stringifyQuery(data)
        options.body = null
      }
    }

    fetch(options.url, {
      method: options.method,
      headers: options.headers,
      body: options.body,
      credentials: 'same-origin'
    })
      .then(response =>
        response
          .text()
          .then(body => {
            let json = {}
            if (body) {
              try {
                json = JSON.parse(body)
              } catch (e) {
                //
              }
            }
            if (!response.ok) {
              if (!json.message) {
                let message = response.statusText
                if (response.status > 499 && response.status < 600) {
                  message = 'Unable to process request. Please try later'
                }
                reject({
                  message,
                  status: response.status
                })
                return
              }
              reject({
                ...json,
                status: response.status
              })
              return
            }
            return json
          })
      )
      .then(resolve)
      .catch(e => {
        if (e.message === 'Failed to fetch') {
          reject(new NetworkError())
          return
        }
        reject(e)
      })
  })
}
