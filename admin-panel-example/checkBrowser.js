import { detect } from 'detect-browser'
const browser = detect();

if (browser) {
  const { name, version } = browser
  const releaseVersion = obtainReleaseVersion(version)
  if (name === 'ie'
    && releaseVersion < 11
    || name === 'safari'
    && releaseVersion < 6
    || name === 'firefox'
    && releaseVersion < 21
    || name === 'chrome'
    && releaseVersion < 23
    || name === 'opera'
    && releaseVersion < 15
    || name === 'android'
    && releaseVersion < 4.4
    || name === 'samsung'
    && releaseVersion < 4
  ) {
    renderOutdatedBrowserNotice()
  }
}

function renderOutdatedBrowserNotice() {
  const node = document.createElement('div')
  node.className += ' error'
  node.innerHTML = '<div class="error__content"><h3>You should use the latest version of the modern browser</h3></div>'
  const bodyNode = document.querySelector('body')
  bodyNode.appendChild(node)
  const appNode = document.getElementById('app')
  bodyNode.removeChild(appNode)
}

function obtainReleaseVersion(version) {
  if (!version) {
    return null
  }
  const versionPieces = version.split('.')
  let result = version
  if (versionPieces.length > 1) {
    result = versionPieces[0] + '.' + versionPieces[1]
  }
  if (isNaN(result)) {
    return null
  }
  return Number(result)
}
