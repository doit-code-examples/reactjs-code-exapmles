import { request } from '../services/api'
import App from '../config/App'

export function update(id, data) {
  return request(`/companies/${id}`, 'PUT', data)
}

export function remove(id) {
  return request(`/companies/${id}`, 'DELETE')
}

export function get(id, query) {
  return request(`/companies/${id}`, 'GET', query)
}

export function getSubscription(id, query) {
  return request(`/subscriptions/${id}`, 'GET', query)
}

export function addAddress(data) {
  return request(`/companies/${new App().getCompany().id}/addresses`, 'POST', data)
}

export function addSubscription(data) {
  return request(`/companies/${new App().getCompany().id}/subscriptions`, 'POST', data)
}
