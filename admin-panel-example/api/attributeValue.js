import {request} from '../services/api'
import App from '../config/App'

export function list(query) {
  return request(`/companies/${new App().getCompany().id}/attribute-values`, 'GET', query)
}

export function create(data) {
  return request(`/companies/${new App().getCompany().id}/attribute-values`, 'POST', data)
}

export function get(id) {
  return request(`/companies/${new App().getCompany().id}/attribute-values/${id}`)
}

export function setBulk(attributeValues) {
  return request(`/companies/${new App().getCompany().id}/attribute-values/bulk`, 'PUT', {
    attributeValues
  })
}
