import {request} from '../services/api'

export function create(data) {
  return request('/addresses', 'POST', data)
}

export function update(id, data) {
  return request(`/addresses/${id}`, 'PUT', data)
}

export function remove(id) {
  return request(`/addresses/${id}`, 'DELETE')
}
