import {request} from '../services/api'

export function list() {
  return request('/attributes')
}

export function get(id) {
  return request(`/attributes/${id}`)
}
