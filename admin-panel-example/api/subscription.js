import { request } from '../services/api'

export function update(id, data) {
  return request(`/subscriptions/${id}`, 'PUT', data)
}

export function pause(id) {
  return request(`/subscriptions/${id}/pause`, 'POST')
}

export function resume(id) {
  return request(`/subscriptions/${id}/resume`, 'POST')
}
