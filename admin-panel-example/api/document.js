import {request} from '../services/api'

export function remove(id) {
  return request(`/documents/${id}`, 'DELETE')
}

export function categories() {
  return request('/documents/categories')
}
