import { request } from '../services/api'
import App from '../config/App'

function getBaseUrl() {
  return `/companies/${new App().getCompany().id}`
}

export function categories(query) {
  return request(`${getBaseUrl()}/analytics/categories`, 'GET', query)
}

export function views(query) {
  return request(`${getBaseUrl()}/analytics/views`, 'GET', query)
}

export function rating(query) {
  return request(`${getBaseUrl()}/analytics/rating`, 'GET', query)
}
