import {request} from '../services/api'

let countries

export function list() {
  if (countries) {
    return Promise.resolve({ countries })
  }
  return request('/countries')
    .then(response => {
      countries = response.countries
      return response
    })
}
