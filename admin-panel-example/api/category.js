import {request} from '../services/api'

export function list(query) {
  return request('/categories', 'GET', query)
}

export function get(id, query) {
  return request(`/categories/${id}`, 'GET', query)
}
