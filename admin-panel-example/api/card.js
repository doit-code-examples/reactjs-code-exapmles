import {request} from '../services/api'
import App from '../config/App';

export function list(query) {
  return request(`/companies/${new App().getCompany().id}/cards`, 'GET', query)
}

export function create(card) {
  card.companyId = new App().getCompany().id
  return request('/cards', 'POST', card)
}

export function update(id, card) {
  return request(`/cards/${id}`, 'PUT', card)
}

export function remove(id) {
  return request(`/cards/${id}`, 'DELETE')
}

export function setAttributes(cardId, valueIds) {
  return request(`/cards/${cardId}/attributes`, 'PUT', { valueIds })
}

export function get(cardId) {
  return request(`/cards/${cardId}`)
}

export function attributeValues(cardId) {
  return request(`/cards/${cardId}/attributes`)
}

export function documents(cardId) {
  return request(`/cards/${cardId}/documents`)
}

export function documentLink(documentId) {
  return request(`/documents/${documentId}/download-link`)
}
