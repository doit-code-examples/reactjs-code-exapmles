import {request} from '../services/api'

export function list(query) {
  return request('/users', 'GET', query)
}

export function update(id, data) {
  return request(`/users/${id}`, 'PUT', data)
}

export function remove(id) {
  return request(`/users/${id}`, 'DELETE')
}

export function get(id, query) {
  return request(`/users/${id}`, 'GET', query)
}

export function create(data) {
  return request('/users', 'POST', data)
}
