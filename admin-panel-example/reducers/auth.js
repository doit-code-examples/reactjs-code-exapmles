const defaultState = {
    user: null,
}

export const Types = {
    AUTHORIZE: 'AUTHORIZE'
}

export default function layout(state = defaultState, action) {
    switch (action.type) {
        case Types.AUTHORIZE:
            return {
                user: action.user
            }

        default:
            return state
    }
}
