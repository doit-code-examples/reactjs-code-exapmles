const defaultState = {
}

export const Types = {
  COMPANY: 'COMPANY'
}

export default function layout(state = defaultState, action) {
  switch (action.type) {
    case Types.COMPANY:
      return {
        ...state,
        ...action.company
      }

    default:
      return state
  }
}
