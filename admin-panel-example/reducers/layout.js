const defaultState = {
  showConfirmationModal: false,
  onLeaveConfirm: null,
  isActionRequiredBeforeLeave: false,
}

export const Types = {
  TOGGLE_SHOWING_CONFIRMATION: 'TOGGLE_SHOWING_CONFIRMATION',
  SET_ACTION_REQUIRED_BEFORE_LEAVE: 'SET_ACTION_REQUIRED_BEFORE_LEAVE',
  UNSET_ACTION_REQUIRED_BEFORE_LEAVE: 'UNSET_ACTION_REQUIRED_BEFORE_LEAVE',
}

export default function layout(state = defaultState, action) {
  switch (action.type) {
    case Types.TOGGLE_SHOWING_CONFIRMATION:
      const showConfirmationModal = !state.showConfirmationModal
      let onLeaveConfirm = null
      if (showConfirmationModal) {
        onLeaveConfirm = action.data.onLeaveConfirm
      }
      return {
        ...state,
        showConfirmationModal,
        onLeaveConfirm,
      }

    case Types.SET_ACTION_REQUIRED_BEFORE_LEAVE:
      return {
        ...state,
        isActionRequiredBeforeLeave: true,
      }

    case Types.UNSET_ACTION_REQUIRED_BEFORE_LEAVE:
      return {
        ...state,
        isActionRequiredBeforeLeave: false,
      }

    default:
      return state
  }
}
