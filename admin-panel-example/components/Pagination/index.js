import React from 'react'

export default function Pagination({
  total,
  perPage,
  page,
  onChangePage,
  className
}) {

  function renderItem(page, isCurrent) {
    return <button
      key={page}
      className={'btn btn-white btn-sm' + (isCurrent ? ' active' : '')}
      onClick={() => {
        if (!isCurrent) {
          onChangePage(page)
        }
      }}>
      {page}
    </button>
  }

  function renderPlaceholder(key) {
    return <span key={key} className="pagination-link"> … </span>
  }

  const pagesCount = Math.ceil(total / perPage)
  const items = []

  for (let i = 1; i <= pagesCount; i++) {
    const diff = Math.abs(page - i)
    if (i === 1 || i === pagesCount || ((page === 1 || page === pagesCount) && Math.abs(diff) < 3) || diff < 2) {
      if (i == page) {
        items.push(renderItem(i, true))
      } else {
        items.push(renderItem(i))
      }
    } else if (i === 2 || i === pagesCount - 1) {
      items.push(renderPlaceholder(i))
    }
  }

  return pagesCount > 1
    ? <div className="d-table ml-auto btn-group">
      <button
        className="btn btn-white btn-sm"
        onClick={() => onChangePage(page - 1)}
        disabled={page == 1}
      >
        Previous
      </button>
      {items}
      <button
        className="btn btn-white btn-sm"
        onClick={() => onChangePage(page + 1)}
        disabled={total / (page * perPage) <= 1}
      >
        Next
      </button>
    </div>
    : null;
}
