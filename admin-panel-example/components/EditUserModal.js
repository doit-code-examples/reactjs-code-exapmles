import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  FormGroup,
  FormInput,
  Form
} from 'shards-react'
import FormWrapper from './Form'
import pick from 'lodash/pick'
import { length, required, email, firstName, lastName } from './Form/validators';
import { update } from '../api/user';
import { fillWithErrors } from './Form/utils'
import { unsetActionRequiredBeforeLeave, setActionRequiredBeforeLeave } from '../actions/layout';

export default class EditUserModal extends Component {
  state = {
    open: true,
    fields: {
      firstName: {
        validator: [required, length(2, 255), firstName],
      },
      lastName: {
        validator: [required, length(1, 255), lastName],
      },
      email: {
        validator: [required, email],
      },
    },
    isChanged: false,
    isLoading: false,
  }

  componentDidMount() {
    const { fields } = this.state

    this.setState({
      fields: Object.keys(fields).reduce((acc, field) => {
        acc[field] = {
          ...fields[field],
          value: this.props.user[field]
        }
        return acc
      }, {})
    })
  }

  toggle = () => {
    this.setState({
      open: !this.state.open
    })
    if (this.state.open
      && typeof this.props.onCancel === 'function'
    ) {
      this.props.onCancel()
    }
  }

  onSubmit = (data) => {
    if (!data) {
      return
    }
    this.setState({ isLoading: true, error: null })
    update(this.props.user.id, data)
      .then(() => {
        this.props.dispatch(unsetActionRequiredBeforeLeave())
        this.props.onUpdated(data)
        this.setState({ isLoading: false })
      })
      .catch(error => {
        this.setState({ isLoading: false })
        if (!error.fields) {
          this.setState((state) => {
            return {
              fields: {
                ...state.fields,
                firstName: {
                  ...state.fields.firstName,
                  error: error.message
                }
              }
            }
          })
          return
        }
        this.setState((state) => {
          return {
            fields: fillWithErrors(error.fields, state.fields),
          }
        })
      })
  }

  onUpdateField = () => {
    if (!this.state.isChanged) {
      this.props.dispatch(setActionRequiredBeforeLeave())
      this.setState({ isChanged: true })
    }
  }

  render() {
    const { open } = this.state

    return <Modal size="sm" centered open={open} toggle={this.toggle}>
      <div className="modal__close">
        <i className="material-icons" onClick={this.toggle}>
          close
        </i>
      </div>
      <ModalHeader className={'modal__header'}>
        Edit Information
      </ModalHeader>
      <ModalBody className="text-center">
        <FormWrapper
          fields={this.state.fields}
          onSubmit={this.onSubmit}
          onUpdateField={this.onUpdateField}
          render={(getControlProps, onSubmit) => {
            const firstNameInput = getControlProps('firstName')
            const lastNameInput = getControlProps('lastName')
            const emailInput = getControlProps('email')
            return <Form onSubmit={onSubmit}>
              <FormGroup className="text-left">
                <div>
                  <label
                    htmlFor="firstName"
                  >
                    First Name
                  </label>
                  <span className="required-mark"> *</span>
                </div>
                <FormInput
                  id="firstName"
                  placeholder="First Name"
                  value={firstNameInput.value}
                  onChange={(event) => {
                    firstNameInput.onChange(event.target.value.replace(' ', ''))
                  }}
                  invalid={!!firstNameInput.error}
                />
                {!!firstNameInput.error
                  && <span className="input__error">{firstNameInput.error}</span>
                }
              </FormGroup>
              <FormGroup className="text-left">
                <div>
                  <label
                    htmlFor="lastName"
                  >
                    Last Name
                  </label>
                  <span className="required-mark"> *</span>
                </div>
                <FormInput
                  id="lastName"
                  placeholder="Last Name"
                  {...pick(lastNameInput, ['value', 'onChange'])}
                  invalid={!!lastNameInput.error}
                />
                {!!lastNameInput.error
                  && <span className="input__error">{lastNameInput.error}</span>
                }
              </FormGroup>
              <FormGroup className="text-left">
                <div>
                  <label
                    htmlFor="email"
                  >
                    Email
                  </label>
                  <span className="required-mark"> *</span>
                </div>
                <FormInput
                  id="email"
                  placeholder="Email"
                  {...pick(emailInput, ['value', 'onChange'])}
                  invalid={!!emailInput.error}
                />
                {!!emailInput.error
                  && <span className="input__error">{emailInput.error}</span>
                }
              </FormGroup>
              <div className="modal__buttons">
                <Button
                  theme="light"
                  pill
                  className="mr-4"
                  type="button"
                  disabled={this.state.isLoading}
                  onClick={this.toggle}
                >
                  Cancel
                </Button>
                <Button
                  disabled={!this.state.isChanged || this.state.isLoading}
                  theme='accent'
                  pill
                  type="submit"
                >
                  Edit
                </Button>
              </div>
            </Form>
          }}
        />
      </ModalBody>
    </Modal>
  }

  componentWillUnmount() {
    this.props.dispatch(unsetActionRequiredBeforeLeave())
  }
}
