import React, { PureComponent } from 'react';
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  FormInput,
  ButtonGroup,
  Button,
  CardFooter
} from 'shards-react'
import { Link } from 'react-router-dom'
import PageTitle from '../PageTitle'
import { list, remove } from '../../api/user'
import App from '../../config/App';
import Pagination from '../Pagination';
import EditModal from '../EditUserModal';
import ConfirmationModal, { Levels } from '../ConfirmationModal';
import StarsRating from '../StarsRating';
import { connect } from 'react-redux'
import RoleCode from '../../enums/RoleCode';
import debounce from 'lodash/debounce';

class Marketers extends PureComponent {
  state = {
    marketers: [],
    meta: {},
    query: {},
    editModalOpenFor: null,
    showRemoveConfirmationFor: null,
  }

  componentDidMount() {
    this.fetchData()
  }

  fetchData() {
    list({
      ...this.state.query,
      roleCode: [RoleCode.MARKETER, RoleCode.MARKETING_ADMIN].join(','),
      company: new App().getCompany().id,
      include: 'cardsCount,averageRating',
    })
      .then(({ users: marketers, meta }) => {
        this.setState({ marketers, meta })
      })
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.query !== prevState.query) {
      this.fetchData()
    }
  }

  updateSearchTerm = debounce(term => {
    if (term === this.state.query.term) {
      return
    }
    this.setState({
      query: {
        ...this.state.query,
        term,
        page: 1
      }
    })
  }, 200)

  onChangeSearch = (event) => {
    this.updateSearchTerm(event.target.value)
  }

  onColumnNameClick = (name) => () => {
    let sort
    if (this.state.query.sort) {
      const pieces = this.state.query.sort.split(' ')
      const field = pieces[0]
      const value = pieces[1] || 'asc'
      if (name === field) {
        if (value === 'desc') {
          sort = void 0
        } else {
          sort = `${field} desc`
        }
      } else {
        sort = `${name} asc`
      }
    } else {
      sort = `${name} asc`
    }
    this.setState({ query: { ...this.state.query, sort, page: 1 } })
  }

  onChangePage = (page) => {
    this.setState(state => {
      return {
        query: {
          ...state.query,
          page
        }
      }
    })
  }

  onUpdated = (data) => {
    this.setState({
      marketers: this.state.marketers.map(marketer => {
        if (marketer.id === this.state.editModalOpenFor) {
          return {
            ...marketer,
            ...data
          }
        }
        return marketer
      }),
      editModalOpenFor: null
    })
  }

  openEditModal = (userId) => () => {
    this.setState({ editModalOpenFor: userId })
  }

  closeEditModal = () => {
    this.setState({ editModalOpenFor: null })
  }

  openDeleteModal = (userId) => () => {
    this.setState({ showRemoveConfirmationFor: userId })
  }

  onRemoveCancel = () => {
    this.setState({ showRemoveConfirmationFor: null })
  }

  remove = () => {
    remove(this.state.showRemoveConfirmationFor)
      .then(() => {
        this.setState(state => {
          return {
            marketers: state.marketers
              .filter(marketer => marketer.id !== state.showRemoveConfirmationFor),
            showRemoveConfirmationFor: null,
            query: {
              ...state.query,
              page: 1
            }
          }
        })
      })
      .catch(() => {})
  }

  renderSortableHeadlineColumn(name, field, sortableField, direction) {
    let className = 'sorting'
    if (sortableField === field && direction) {
      className += ` sorting_${direction}`
    }
    return <th
      className={className}
      onClick={this.onColumnNameClick(field)}
    >
      {name}
    </th>
  }

  render() {
    const page = this.state.query.page || 1
    const from = (page - 1) * this.state.meta.perPage + 1
    const to = from - 1 + this.state.marketers.length
    let field
    let direction
    if (this.state.query.sort) {
      const pieces = this.state.query.sort.split(' ')
      field = pieces[0]
      direction = pieces[1] || 'asc'
    }
    const user = this.props.auth.user
    const isAuthorizedUserMarketingAdmin = this.props.auth.user
      && user.userRoles.find(role => {
        return role.companyId === new App().getCompany().id && role.role.code === RoleCode.MARKETING_ADMIN
      })

    return <Container fluid className="main-content-container px-4">
      <Row noGutters className="page-header py-4">
        <PageTitle
          sm="4"
          title="Marketing Team"
          className="text-sm-left mb-3"
        />
      </Row>
      <Card small className="mb-4 resources-listing">
        <CardHeader className="border-bottom">
          <Row className="search-header">
            <Col className="search-header__input">
              <InputGroup seamless size="sm" className="ml-auto">
                <InputGroupAddon type="prepend">
                  <InputGroupText>
                    <i className="material-icons">search</i>
                  </InputGroupText>
                </InputGroupAddon>
                <FormInput onChange={this.onChangeSearch} placeholder="Search..." />
              </InputGroup>
            </Col>
            <Col className="search-header__button">
              <Link
                className="btn btn-white active btn-pill"
                to="/marketers/add"
              >
                Add Marketing User
              </Link>
            </Col>
          </Row>
        </CardHeader>
        <CardBody className="px-0 pt-0 pb-0">
          {this.state.meta.total < 1
            ? <div
              className="empty-placeholder"
            >
              <h3>No results found</h3>
              <p>By this criteria has not results yet.</p>
            </div>
            : <div className="table-responsive dataTables_wrapper">
              <table className="ui celled table dataTable">
                <thead>
                  <tr>
                    <th>#</th>
                    {this.renderSortableHeadlineColumn('Name', 'firstName', field, direction)}
                    <th>Email</th>
                    {this.renderSortableHeadlineColumn('Amount of QCards', 'cardsCount', field, direction)}
                    {this.renderSortableHeadlineColumn('Rating', 'averageRating', field, direction)}
                    {isAuthorizedUserMarketingAdmin
                      && <th>Actions</th>
                    }
                  </tr>
                </thead>
                <tbody>
                  {this.state.marketers.map((marketer, key) => {
                    return <tr
                      key={marketer.id}
                    >
                      <td>{(page - 1) * this.state.meta.perPage + key + 1}</td>
                      <td>
                        <Link to={`/marketers/${marketer.id}`}>
                          {`${marketer.firstName} ${marketer.lastName}`}
                        </Link>
                      </td>
                      <td>
                        {marketer.email}
                      </td>
                      <td>
                        {marketer.cardsCount}
                      </td>
                      <td>
                        {marketer.averageRating
                          && <StarsRating
                            rating={marketer.averageRating}
                          />
                        }

                      </td>
                      {isAuthorizedUserMarketingAdmin
                        && <td>
                          <ButtonGroup size="sm">
                            <Button
                              theme="white"
                              onClick={this.openEditModal(marketer.id)}
                            >
                              <i className="material-icons">&#xE254;</i>
                            </Button>
                            {marketer.id !== user.id
                              && <Button
                                outline
                                theme="danger"
                                onClick={this.openDeleteModal(marketer.id)}
                              >
                                <i className="material-icons">delete_forever</i>
                              </Button>
                            }
                          </ButtonGroup>
                        </td>
                      }
                    </tr>

                  })}
                </tbody>
              </table>
            </div>
          }

        </CardBody>
        {this.state.meta.total > 0
          && <CardFooter>
            <div className="resources-listing__pagination">
              <div className="resources-listing__pagination-info">
                {this.state.meta.total > 1
                  ? `Showing ${from} to ${to} of ${this.state.meta.total} entries`
                  : 'Showing 1 entry'
                }
              </div>
              <div className="resources-listing__pagination-navigation">
                <Pagination
                  page={this.state.query.page || 1}
                  total={this.state.meta.total}
                  perPage={this.state.meta.perPage}
                  onChangePage={this.onChangePage}
                />
              </div>
            </div>
          </CardFooter>
        }
      </Card>
      {this.state.editModalOpenFor
        && <EditModal
          user={this.state.marketers.find(marketer => marketer.id === this.state.editModalOpenFor)}
          onUpdated={this.onUpdated}
          onCancel={this.closeEditModal}
          dispatch={this.props.dispatch}
        />
      }
      {this.state.showRemoveConfirmationFor
        && <ConfirmationModal
          onConfirm={this.remove}
          onCancel={this.onRemoveCancel}
          message="Are you sure you want to delete this Marketer?"
          confirmButtonText="Delete"
          level={Levels.CRITICAL}
        />
      }
    </Container>
  }
}

export default connect(state => ({
  auth: state.auth
}))(Marketers)
