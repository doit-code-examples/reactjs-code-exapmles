import React, { PureComponent } from 'react'
import { connect } from 'react-redux';
import {
  Form,
  Container,
  Row,
  Col,
  Card,
  CardBody,
  FormGroup,
  FormInput,
  Button,
  Alert,
  CardHeader
} from 'shards-react'
import PageTitle from '../PageTitle'
import FormWrapper from '../Form'
import pick from 'lodash/pick'
import omit from 'lodash/omit'
import { required, length, email, firstName, lastName } from '../Form/validators'
import { unsetActionRequiredBeforeLeave, setActionRequiredBeforeLeave } from '../../actions/layout';
import { create } from '../../api/user';
import { fillWithErrors } from '../Form/utils';
import App from '../../config/App';
import RoleCode from '../../enums/RoleCode';
import SuccessMessage from '../SuccessMessage';

class AddMarketer extends PureComponent {
  state = {
    fields: {
      firstName: {
        validator: [required, length(2, 255), firstName],
      },
      lastName: {
        validator: [required, length(1, 255), lastName],
      },
      email: {
        validator: [required, email],
      },
    },
    isChanged: false,
    isSaved: false,
    error: null,
    isLoading: false,
  }

  onSubmit = (data) => {
    if (!data) {
      return
    }

    this.setState({
      error: null,
      isLoading: true,
    })

    create({
      ...data,
      roleCode: RoleCode.MARKETER,
      companyId: new App().getCompany().id
    })
      .then(() => {
        this.setState({ isSaved: true, isLoading: false })
        this.props.dispatch(unsetActionRequiredBeforeLeave())
      })
      .catch(error => {
        this.setState({
          isLoading: false
        })
        if (!error.fields) {
          this.setState({
            error: error.message
          })
          return
        }
        this.setState((state) => {
          return {
            fields: fillWithErrors(error.fields, state.fields, 'email')
          }
        })
      })
  }

  onUpdateField = (updatedField, value) => {
    const fields = Object.keys(this.state.fields).reduce((acc, field) => {
      if (updatedField === field) {
        acc[field] = omit(this.state.fields[field], 'error')
        return acc
      }
      acc[field] = this.state.fields[field]
      return acc
    }, {})

    if (!this.state.isChanged) {
      this.props.dispatch(setActionRequiredBeforeLeave())
    }
    this.setState({ isChanged: true, fields })
  }

  goBack = () => {
    this.props.history.push('/marketers')
  }

  onClickToHideError = () => {
    this.setState({ error: null })
  }

  render() {
    return <Container fluid className="main-content-container px-4">
      <Row noGutters className="page-header py-4">
        <PageTitle sm="4" title="Add Marketing User" subtitle="Marketing Team" className="text-sm-left" />
      </Row>
      <Row>
        <Col>
          {this.state.isSaved
            ? <SuccessMessage
              message="The invitation was successfully sent to the specified email"
              onButtonClick={this.goBack}
            />
            : <Card small className="mb-4 add-marketer">
              <CardHeader>
                <h5 className="m-0">General</h5>
              </CardHeader>
              <CardBody>
                <FormWrapper
                  fields={this.state.fields}
                  onSubmit={this.onSubmit}
                  onUpdateField={this.onUpdateField}
                  render={(getControlProps, onSubmit) => {
                    const firstNameInput = getControlProps('firstName')
                    const lastNameInput = getControlProps('lastName')
                    const emailInput = getControlProps('email')
                    return <Form onSubmit={onSubmit}>
                      {this.state.error
                        && <Alert
                          theme="danger"
                          dismissible={this.onClickToHideError}
                        >
                          {this.state.error}
                        </Alert>
                      }
                      <Row>
                        <FormGroup className="text-left col col-6">
                          <div>
                            <label
                              htmlFor="firstName"
                            >
                              First Name
                            </label>
                            <span className="required-mark"> *</span>
                          </div>
                          <FormInput
                            id="firstName"
                            placeholder="First Name"
                            {...pick(firstNameInput, ['value', 'onChange'])}
                            invalid={!!firstNameInput.error}
                          />
                          {!!firstNameInput.error
                            && <span className="input__error">{firstNameInput.error}</span>
                          }
                        </FormGroup>
                        <FormGroup className="text-left col col-6">
                          <div>
                            <label
                              htmlFor="lastName"
                            >
                              Last Name
                            </label>
                            <span className="required-mark"> *</span>
                          </div>
                          <FormInput
                            id="lastName"
                            placeholder="Last Name"
                            {...pick(lastNameInput, ['value', 'onChange'])}
                            invalid={!!lastNameInput.error}
                          />
                          {!!lastNameInput.error
                            && <span className="input__error">{lastNameInput.error}</span>
                          }
                        </FormGroup>
                      </Row>
                      <FormGroup className="text-left">
                        <div>
                          <label
                            htmlFor="email"
                          >
                            Email
                          </label>
                          <span className="required-mark"> *</span>
                        </div>
                        <FormInput
                          id="email"
                          type="email"
                          placeholder="Email"
                          {...pick(emailInput, ['value', 'onChange'])}
                          invalid={!!emailInput.error}
                        />
                        {!!emailInput.error
                          && <span className="input__error">{emailInput.error}</span>
                        }
                      </FormGroup>
                      <div className="action-buttons">
                        <Button
                          disabled={!this.state.isChanged || this.state.isLoading}
                          theme='accent'
                          pill
                          type="submit"
                        >
                          Send Invite
                        </Button>
                      </div>
                    </Form>
                  }}
                />

              </CardBody>
            </Card>
          }
        </Col>
      </Row>
    </Container>
  }

  componentWillUnmount() {
    this.props.dispatch(unsetActionRequiredBeforeLeave())
  }
}

export default connect(state => ({}))(AddMarketer)
