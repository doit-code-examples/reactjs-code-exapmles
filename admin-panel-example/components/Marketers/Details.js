import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Badge
} from 'shards-react'
import { get } from '../../api/user'
import PageTitle from '../PageTitle'
import { list } from '../../api/card'
import { Link } from 'react-router-dom'
import moment from 'moment'
import App from '../../config/App'
import PageError from '../PageError';
import Pagination from '../Pagination';

class Details extends PureComponent {
  state = {
    marketer: null,
    cards: [],
    pageError: null,
    meta: {},
    cardsPage: 1
  }

  componentDidMount() {
    const { marketerId } = this.props.match.params
    get(marketerId, {
      include: 'userRoles.[role]'
    })
      .then(({ user: marketer }) => {
        this.setState({ marketer }, () => {
          this.fetchCards(this.state.cardsPage)
        })
      })
      .catch(error => {
        this.setState({ pageError: error })
      })
  }


  componentDidUpdate(prevProps, prevState) {
    if (this.state.cardsPage !== prevState.cardsPage) {
      this.fetchCards(this.state.cardsPage)
    }
  }

  fetchCards(cardsPage) {
    const { marketerId } = this.props.match.params
    if (!marketerId) {
      return Promise.resolve()
    }
    list({
      author: marketerId,
      include: 'attributes.value,category',
      sort: 'updatedAt desc',
      page: cardsPage,
      limit: 14
    })
      .then(({ cards, meta }) => {
        this.setState({ cards, meta })
      })
  }

  onChangePage = (cardsPage) => {
    this.setState({ cardsPage })
  }

  render() {
    const { marketer, pageError, cardsPage } = this.state
    const from = (cardsPage - 1) * this.state.meta.perPage + 1
    const to = from - 1 + this.state.cards.length

    if (pageError) {
      return <PageError error={pageError} />
    }
    let role
    if (marketer) {
      role = (marketer.userRoles.find(role => {
        return role.companyId === new App().getCompany().id
      }) || {}).role.name
    }

    return <Container fluid className="main-content-container px-4">
      <Row noGutters className="page-header py-4">
        <PageTitle sm="4" title="Profile" subtitle="Marketing Team" className="text-sm-left" />
      </Row>
      {marketer
        &&
        <Row>
          <Col lg="4" md="12" className="mb-4">
            <Card small className="resource-details">
              <CardHeader className="p-0 user-details__header">
                <div className="user-details__avatar-wrapper">
                  <div className="user-details__avatar"></div>
                </div>
              </CardHeader>
              <CardBody className="light-text">
                <h5 className="resource__name">{marketer.firstName} {marketer.lastName}</h5>
                <p>{role}</p>
                <div>
                  <span className="input__label">Company</span>
                  <p>{new App().getCompany().name}</p>
                </div>
                <div>
                  <span className="input__label">Email</span>
                  <p>{marketer.email}</p>
                </div>
                {marketer.phone
                  && <div>
                    <span className="input__label">Phone</span>
                    <p>{marketer.phone}</p>
                  </div>
                }

              </CardBody>
            </Card>

          </Col>
          {this.state.cards.length > 0
            && <Col lg="8" md="12">
              <Card small className="resources-listing">
                <CardHeader>
                  <h5 className="m-0">QCards Updates</h5>
                </CardHeader>
                <CardBody >
                  <Row>
                    {this.state.cards.map(card => {
                      const productAttribute = card.attributes.find(attr => attr.value.attributeId === 1)
                      return <Col lg="6" md="6" key={card.id}>
                        <Card small className={`content-card card-post mb-4 ${card.isPublished ? 'content-card--published' : ''}`}>
                          <Link to={`/content/${card.id}/edit`}>
                            <CardBody>
                              {card.isPublished
                                ? <div className="content-card__status">
                                  <i className="material-icons mr-1 content-card__status-icon--published">visibility</i>
                                  Published
                                </div>
                                : <div className="content-card__status">
                                  <i className="material-icons mr-1 content-card__status-icon">flag</i>
                                  Draft
                                </div>
                              }
                              <h5 className="card-title">{card.title}</h5>
                              <p className="card-text text-muted">
                                {card.content}
                              </p>
                            </CardBody>
                          </Link>
                          <CardFooter className="border-top d-flex">
                            <div className="card-post__author d-flex">
                              <div
                                className="card-post__author-avatar card-post__author-avatar--small"
                                style={{
                                  backgroundImage: 'none'
                                }}>
                              </div>
                              <div className="d-flex flex-column justify-content-center ml-3 card-post__meta">
                                <span className="card-post__author-name">
                                  {[this.state.marketer.firstName, this.state.marketer.lastName].join(' ')}
                                </span>
                                <small className="text-muted">
                                  {moment(card.updatedAt || card.createdAt).local().format('D MMMM YYYY')}
                                </small>
                              </div>
                            </div>
                            <div className="cards__attributes">
                              <Link to={`/content?category=${card.categoryId}`}>
                                <Badge
                                  pill
                                  className="cards__attributes-item"
                                  style={{
                                    backgroundColor: `#${card.category.color}`
                                  }}
                                >
                                  {card.category.name}
                                </Badge>
                              </Link>
                              {productAttribute
                                ? <Link to={`/content?type=products&a-product=${productAttribute.value.id}`}>
                                  <Badge
                                    pill
                                    className="cards__attributes-item bg-royal-blue"
                                  >
                                    {productAttribute.value.value}
                                  </Badge>
                                </Link>
                                : <Badge
                                  pill
                                  className="cards__attributes-item bg-royal-blue"
                                >
                                  Undefined
                                </Badge>
                              }
                            </div>
                          </CardFooter>
                        </Card>
                      </Col>
                    })}

                  </Row>
                </CardBody>
                {this.state.meta.total > 0
                  && <CardFooter>
                    <div className="resources-listing__pagination">
                      <div className="resources-listing__pagination-info">
                        {this.state.meta.total > 1
                          ? `Showing ${from} to ${to} of ${this.state.meta.total} entries`
                          : 'Showing 1 entry'
                        }
                      </div>
                      <div className="resources-listing__pagination-navigation">
                        <Pagination
                          page={this.state.cardsPage}
                          total={this.state.meta.total}
                          perPage={this.state.meta.perPage}
                          onChangePage={this.onChangePage}
                        />
                      </div>
                    </div>
                  </CardFooter>
                }

              </Card>
            </Col>
          }
        </Row>
      }
    </Container>
  }
}

export default connect(state => ({}))(Details)

