import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import {
  Row,
  Container,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  FormInput,
  Button,
  Alert
} from 'shards-react'
import PageTitle from '../PageTitle'
import FormWrapper from '../Form'
import pick from 'lodash/pick'
import omit from 'lodash/omit'
import { required, length } from '../Form/validators';
import { update } from '../../api/user';
import { fillWithErrors } from '../Form/utils';
import App from '../../config/App';
import RoleCode from '../../enums/RoleCode';
import SuccessMessage from '../SuccessMessage';
import {
  setActionRequiredBeforeLeave,
  unsetActionRequiredBeforeLeave
} from '../../actions/layout';

class Settings extends PureComponent {
  state = {
    fields: {
      currentPassword: {
        validator: [required, length(0, 255)]
      },
      password: {
        validator: [required, length(6, 255), (value, fields) => {
          if (fields['currentPassword'] && value === fields['currentPassword']) {
            return 'The new password should be different from a current password'
          }
          return false
        }]
      },
      repeatPassword: {
        validator: [required, length(6, 255), (value, fields) => {
          if (fields['password'] && value !== fields['password']) {
            return 'The value should be the same as for a new password'
          }
          return false
        }]
      },
    },
    isChanged: false,
    isSaved: false,
    isLoading: false,
    error: null,
  }

  company

  constructor(props) {
    super(props)
    this.company = new App().getCompany()
  }

  onSubmit = (data) => {
    if (!data) {
      return
    }

    const { currentPassword, password } = data
    this.setState({ isLoading: true, error: null })
    update(this.props.auth.user.id, {
      currentPassword,
      password,
    })
      .then(() => {
        this.props.dispatch(unsetActionRequiredBeforeLeave())
        this.setState({
          isLoading: false,
          isChanged: false,
          isSaved: true,
          fields: Object.keys(this.state.fields).reduce((acc, field) => {
            acc[field] = {
              ...this.state.fields[field],
              value: ''
            }
            return acc
          }, {})
        })
      })
      .catch((error) => {
        this.setState({ isLoading: false })
        if (!error.fields) {
          this.setState((state) => {
            return {
              error: error.message
            }
          })
          return
        }
        this.setState((state) => {
          return {
            fields: fillWithErrors(error.fields, state.fields)
          }
        })
      })
  }

  onUpdateField = (updatedField, value) => {
    const fields = Object.keys(this.state.fields).reduce((acc, field) => {
      if (updatedField === field) {
        acc[field] = {
          ...omit(this.state.fields[field], 'error'),
          value
        }
        return acc
      }
      acc[field] = this.state.fields[field]
      return acc
    }, {})
    this.setState({ isChanged: true, fields })
    if (!this.state.isChanged) {
      this.props.dispatch(setActionRequiredBeforeLeave())
    }
  }

  goBack = () => {
    this.setState({ isSaved: false })
  }

  onClickToHideError = () => {
    this.setState({ error: null })
  }

  render() {
    const { user } = this.props.auth

    return <Container fluid className="main-content-container px-4">
      <Row noGutters className="page-header py-4">
        <PageTitle sm="4" title="Profile Info" subtitle="Settings" className="text-sm-left" />
      </Row>
      {this.state.isSaved
        ? <Row>
          <Col>
            <SuccessMessage
              message="The changes successfully saved"
              onButtonClick={this.goBack}
            />
          </Col>
        </Row>
        : <Row>
          <Col lg="7" md="12" className="mb-4 resource-details">
            <Card small>
              <CardHeader className="p-0 user-details__header">
                <div className="user-details__avatar-wrapper">
                  <div className="user-details__avatar"></div>
                </div>
              </CardHeader>
              <CardBody className="light-text">
                <div className="resource-details__headline">
                  <h5>{user.firstName} {user.lastName}</h5>
                </div>
                {user.userRoles.length > 0
                  && <p>
                    {user.userRoles.find(userRole => {
                      return userRole.companyId === this.company.id
                        && userRole.role.code !== RoleCode.SALESPERSON
                    }).role.name}
                  </p>
                }
                <div>
                  <span className="input__label input__label--settings">Email</span>
                  <div className="field__value">{user.email}</div>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col lg="5" md="12" className="mb-4">
            <Card small>
              <CardHeader>
                <h5 className="m-0">Change Your Password</h5>
              </CardHeader>
              <CardBody>
                {this.state.error
                  && <Alert
                    theme="danger"
                    dismissible={this.onClickToHideError}
                  >
                    {this.state.error}
                  </Alert>
                }
                <FormWrapper
                  fields={this.state.fields}
                  onSubmit={this.onSubmit}
                  onUpdateField={this.onUpdateField}
                  render={(getControlProps, onSubmit) => {
                    const currentPasswordProps = getControlProps('currentPassword')
                    const passwordProps = getControlProps('password')
                    const repeatPasswordProps = getControlProps('repeatPassword')
                    return <form onSubmit={onSubmit}>
                      <FormGroup className="text-left">
                        <div>
                          <label
                            htmlFor="currentPassword"
                          >
                            Current Password
                          </label>
                          <span className="required-mark"> *</span>
                        </div>
                        <FormInput
                          id="currentPassword"
                          autoComplete="current-password"
                          placeholder="Enter Current Password"
                          type="password"
                          {...pick(currentPasswordProps, ['value', 'onChange'])}
                          invalid={!!currentPasswordProps.error}
                        />
                        {!!currentPasswordProps.error
                          && <span className="input__error">{currentPasswordProps.error}</span>
                        }
                      </FormGroup>
                      <FormGroup className="text-left">
                        <div>
                          <label
                            htmlFor="password"
                          >
                            New Password
                          </label>
                          <span className="required-mark"> *</span>
                        </div>
                        <FormInput
                          id="password"
                          autoComplete="new-password"
                          type="password"
                          placeholder="Enter New Password"
                          {...pick(passwordProps, ['value', 'onChange'])}
                          invalid={!!passwordProps.error}
                        />
                        {!!passwordProps.error
                          && <span className="input__error">{passwordProps.error}</span>
                        }
                      </FormGroup>
                      <FormGroup className="text-left">
                        <div>
                          <label
                            htmlFor="repeatPassword"
                          >
                            Repeat New Password
                          </label>
                          <span className="required-mark"> *</span>
                        </div>
                        <FormInput
                          id="repeatPassword"
                          type="password"
                          autoComplete="new-password"
                          placeholder="Repeat New Password"
                          {...pick(repeatPasswordProps, ['value', 'onChange'])}
                          invalid={!!repeatPasswordProps.error}
                        />
                        {!!repeatPasswordProps.error
                          && <span className="input__error">{repeatPasswordProps.error}</span>
                        }
                      </FormGroup>
                      <Button
                        disabled={!this.state.isChanged || this.state.isLoading}
                        theme='accent'
                        pill
                        className="ml-auto"
                        type="submit"
                      >
                        Change Password
                      </Button>
                    </form>
                  }}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      }
    </Container>
  }

  componentWillUnmount() {
    this.props.dispatch(unsetActionRequiredBeforeLeave())
  }
}

export default connect(state => ({
  auth: state.auth
}))(Settings)
