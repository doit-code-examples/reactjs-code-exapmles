import React, { PureComponent } from 'react'
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  CardFooter,
  FormSelect
} from 'shards-react'
import { Link } from 'react-router-dom'
import { views } from '../../api/analytics'
import { shorthandedNumber } from '../../utils/formatting'

export default class Views extends PureComponent {
  state = {
    cards: [],
    categories: [],
    documents: [],
    period: 'week'
  }

  componentDidMount() {
    this.fetchData(this.state.period)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.period !== prevState.period) {
      this.fetchData(this.state.period)
    }
  }

  fetchData(period) {
    views({
      period
    })
      .then(({ analytics }) => {
        this.setState({
          cards: analytics.cards,
          categories: analytics.categories,
          documents: analytics.documents,
        })
      })
  }

  onChangePeriod = (event) => {
    const { value } = event.target
    this.setState({
      period:  value == '0'
        ? null
        : value
    })
  }

  render() {
    const { cards, categories, documents } = this.state
    return <Card small className="p-0 analytics-widget">
      <CardHeader className="border-bottom card__header">
        <h5 className="m-0">Most Viewed</h5>
      </CardHeader>

      <CardBody className="p-0">
        <Row className="m-0">
          <Col sm="4" className="analytics-widget__section">
            <h6 className="analytics-widget__title">Most Viewed QCards</h6>
            <ul>
              {cards.map(card => {
                return <li key={card.id} className="analytics-widget__item">
                  <i className="material-icons mr-1 analytics-widget__icon--views">visibility</i>
                  <span className="analytics-widget__value">{shorthandedNumber(card.views)} </span>
                  <Link to={`/content/${card.id}/edit`}>{card.title}</Link>
                </li>
              })}
            </ul>
          </Col>
          <Col sm="4" className="analytics-widget__section">
            <h6 className="analytics-widget__title">Most Viewed Categories</h6>
            <ul>
              {categories.map(category => {
                return <li key={category.id} className="analytics-widget__item">
                  <span
                    className="analytics-widget__category-color"
                    style={{
                      backgroundColor: `#${category.color}`
                    }}
                  />
                  <span className="analytics-widget__value">{shorthandedNumber(category.views)} </span>
                  <Link to={`/content?category=${category.id}`}>{category.name}</Link>
                </li>
              })}
            </ul>
          </Col>
          <Col sm="4" className="analytics-widget__section">
            <h6 className="analytics-widget__title">Most Viewed Documents</h6>
            <ul>
              {documents.map(document => {
                return <li key={document.id} className="analytics-widget__item">
                  <i className="material-icons mr-1 analytics-widget__icon--views">visibility</i>
                  <span className="analytics-widget__value">{shorthandedNumber(document.views)} </span>
                  {/*<Link to={`/content?category=${document.id}`}>{document.name}</Link>*/}
                  <span>{document.name}</span>
                </li>
              })}
            </ul>
          </Col>
        </Row>
      </CardBody>
      <CardFooter className="border-top">
        <Row>
          <Col>
            <FormSelect
              size="sm"
              value={this.state.period || 0}
              style={{ maxWidth: '130px' }}
              onChange={this.onChangePeriod}
            >
              <option value="day">Last day</option>
              <option value="week">Last week</option>
              <option value="month">Last month</option>
              <option value="0">All time</option>
            </FormSelect>
          </Col>
        </Row>
      </CardFooter>
    </Card>
  }
}
