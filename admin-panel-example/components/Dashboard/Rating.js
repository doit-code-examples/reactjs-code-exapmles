import React, { PureComponent } from 'react'
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  CardFooter,
  FormSelect
} from 'shards-react'
import { rating } from '../../api/analytics'
import { Link } from 'react-router-dom'
import { decimal } from '../../utils/formatting'

export default class Rating extends PureComponent {
  state = {
    cards: [],
    marketers: [],
    documents: [],
    period: 'week'
  }

  componentDidMount() {
    this.fetchData(this.state.period)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.period !== prevState.period) {
      this.fetchData(this.state.period)
    }
  }

  fetchData(period) {
    rating({
      period
    })
      .then(({ analytics }) => {
        this.setState({
          cards: analytics.cards,
          marketers: analytics.marketers,
          documents: analytics.documents,
        })
      })
  }

  onChangePeriod = (event) => {
    const { value } = event.target
    this.setState({
      period: value == '0'
        ? null
        : value
    })
  }

  render() {
    const { cards, marketers, documents } = this.state;
    return <Card small className="p-0 analytics-widget">
      <CardHeader className="border-bottom card__header">
        <h5 className="m-0">Rating</h5>
      </CardHeader>

      <CardBody className="p-0">
        <Row className="m-0">
          <Col sm="4" className="analytics-widget__section">
            <h6 className="analytics-widget__title">Top Rated QCards</h6>
            <ul>
              {cards.map(card => {
                return <li key={card.id} className="analytics-widget__item">
                  <i className="material-icons mr-1 analytics-widget__icon--star">star</i>
                  <span className="analytics-widget__value">
                    {decimal(card.rating, 1)}
                  </span>
                  <Link to={`/content/${card.id}/edit`}>{card.title}</Link>
                </li>
              })}
            </ul>
          </Col>
          <Col sm="4" className="analytics-widget__section">
            <h6 className="analytics-widget__title">Top Marketing Users</h6>
            <ul>
              {marketers.map(marketer => {
                return <li key={marketer.id} className="analytics-widget__item">
                  <i className="material-icons mr-1 analytics-widget__icon--star">star</i>
                  <span className="analytics-widget__value">
                    {decimal(marketer.rating, 1)}
                  </span>
                  <Link to={`/marketers/${marketer.id}`}>{marketer.firstName} {marketer.lastName}</Link>
                </li>
              })}
            </ul>
          </Col>
          <Col sm="4" className="analytics-widget__section">
            <h6 className="analytics-widget__title">Top 5 Documents</h6>
            <ul>
              {documents.map(document => {
                return <li key={document.id} className="analytics-widget__item">
                  <i className="material-icons mr-1 analytics-widget__icon--star">star</i>
                  <span className="analytics-widget__value">
                    {decimal(document.rating, 1)}
                  </span>
                  {/*<Link to={`/marketers/${document.id}`}>{document.name}</Link>*/}
                  <span>{document.name}</span>
                </li>
              })}
            </ul>
          </Col>
        </Row>
      </CardBody>
      <CardFooter className="border-top">
        <Row>
          <Col>
            <FormSelect
              size="sm"
              value={this.state.period || 0}
              style={{ maxWidth: '130px' }}
              onChange={this.onChangePeriod}
            >
              <option value="day">Last day</option>
              <option value="week">Last week</option>
              <option value="month">Last month</option>
              <option value="0">All time</option>
            </FormSelect>
          </Col>
        </Row>
      </CardFooter>
    </Card>
  }
}
