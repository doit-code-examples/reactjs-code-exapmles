import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import {
  Row,
  Container,
  Col,
  Button,
} from 'shards-react'
import { Link } from 'react-router-dom'
import PageTitle from '../PageTitle'
import CategoriesPopularity from './CategoriesPopularity'
import Rating from './Rating';
import Views from './Views';

class Dashboard extends PureComponent {
  render() {
    return <Container fluid className="main-content-container px-4">
      <Row noGutters className="page-header py-4">
        <PageTitle sm="4" title="Home" className="text-sm-left" />
      </Row>
      <Row className="mb-3">
        <Col className="dashboard__buttons">
          <Button
            tag={Link}
            to="/content/add"
            className="dashboard__buttons-item"
            pill
          >
            Add New QCard
          </Button>
          <Button
            tag={Link}
            to="/marketers/add"
            className="dashboard__buttons-item"
            pill
          >
            Add Marketing User
          </Button>
          <Button
            tag={Link}
            to="/salespersons/add"
            pill
            className="dashboard__buttons-item"
          >
            Add Sales User
          </Button>
        </Col>
      </Row>
      <CategoriesPopularity />
      <Row className="mt-4">
        <Col md="12" className="mb-4">
          <Rating />
        </Col>
        <Col md="12" >
          <Views />
        </Col>
      </Row>
    </Container>
  }
}

export default connect(state => ({
}))(Dashboard)
