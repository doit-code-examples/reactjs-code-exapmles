import React, { PureComponent, createRef } from 'react'
import PropTypes from 'prop-types'
import {
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
  ButtonGroup,
  Button
} from 'shards-react'
import Chart from '../../utils/chart'
import { list } from '../../api/category'
import { categories as categoriesAnalytics } from '../../api/analytics'
import moment from 'moment'

const periodsMap = {
  'Day': 'day',
  'Week': 'week',
  'Month': 'month',
  'All time': null
}

const formattingMap = {
  'day': 'hh:mm a',
  'week': 'D MMM',
  'month': 'D MMM',
}

export default class CategoriesPopularity extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
  }

  static defaultProps = {
    title: 'Category Popularity',
  }

  state = {
    period: 'day',
    categories: []
  }

  chart = null

  constructor(props) {
    super(props)

    this.legendRef = createRef()
    this.canvasRef = createRef()
    this.chartWrapper = createRef()
  }

  componentDidMount() {
    list()
      .then(({ categories }) => {
        this.setState({ categories })
        this.fetchAndRender(categories)
      })

    document.addEventListener('click', (event) => {
      if (!this.chart || !this.chartWrapper.current) {
        return
      }
      const clickedInside = this.chartWrapper.current.contains(event.target);
      if (!clickedInside) {
        const tooltip = this.chart.tooltip;
        tooltip._active = [];
        tooltip.update(true);
        this.chart.update();
      }
    })
  }

  getChartOptions() {
    return {
      responsive: true,
      legend: false,
      maintainAspectRatio: false,
      events:  ["click"],
      elements: {
        line: {
          // A higher value makes the line look skewed at this ratio.
          tension: 0.38
        }
      },
      scales: {
        xAxes: [
          {
            gridLines: false,
            ticks: {
              callback(tick, index) {
                return index % 2 === 0 ? '' : tick
              },
              fontColor: '#BBBCC1',
            }
          }
        ],
        yAxes: [
          {
            ticks: {
              maxBarThickness : 5,
              suggestedMax: 100,
              stepSize: 20,
              beginAtZero: true,
              fontColor: '#BBBCC1',
            }
          }
        ]
      },
      tooltips: {
        enabled: false,
        mode: 'index',
        position: 'nearest',
        custom: (tooltip) => {
          const wrapperNode = this.chartWrapper.current
          const itemNode = this.chartWrapper.current.querySelector('.canvas-wrapper')
          let tooltipEl = document.getElementsByClassName(`sc-tooltip-${this.chart.id}`)[0];

          if (!tooltipEl) {
            tooltipEl = document.createElement('div');
            tooltipEl.classList.add(`sc-tooltip-${this.chart.id}`);
            tooltipEl.innerHTML = '<table></table>';
            this.chart.canvas.parentNode.appendChild(tooltipEl);
          }

          // Hide if no tooltip
          if (tooltip.opacity === 0) {
            if (wrapperNode && itemNode) {
              wrapperNode.style['overflow-x'] = 'auto';
              itemNode.style.minWidth = '800px'
            }
            tooltipEl.style.display = 'none';
            return;
          }

          if (wrapperNode && itemNode) {
            wrapperNode.style['overflow-x'] = 'initial';
            itemNode.style.minWidth = 'unset'
          }

          // Set caret Position
          tooltipEl.classList.remove('above', 'below', 'no-transform');
          if (tooltip.yAlign) {
            tooltipEl.classList.add(tooltip.yAlign);
          } else {
            tooltipEl.classList.add('no-transform');
          }

          function getBody(bodyItem) {
            return bodyItem.lines;
          }

          // Set Text
          if (tooltip.body) {
            const titleLines = tooltip.title || [];
            const bodyLines = tooltip.body.map(getBody);
            let innerHtml = '<thead>';

            titleLines.forEach((title) => {
              innerHtml += `<tr><th>${title}</th></tr>`;
            });
            innerHtml += '</thead><tbody>';

            bodyLines.forEach((body, i) => {
              const _colors = tooltip.labelColors[i];
              let style = `background:${_colors.backgroundColor}`;
              style += `; border-color:${_colors.borderColor}`;
              style += '; border-width: 2px';
              const span = `<span class="sc-tooltip-key" style="${style}"></span>`;
              innerHtml += `<tr><td>${span} ${body}</td></tr>`;
            });
            innerHtml += '</tbody>';

            const tableRoot = tooltipEl.querySelector('table');
            tableRoot.innerHTML = innerHtml;
          }

          const positionY = this.chart.canvas.offsetTop;
          const positionX = this.chart.canvas.offsetLeft;

          // Display, position, and set styles for font
          tooltipEl.style.display = 'block';
          tooltipEl.style.left = `${positionX + tooltip.caretX}px`;
          tooltipEl.style.top = `${positionY + tooltip.caretY}px`;
        }
      },
    }
  }

  fetchAndRender(categories) {
    categoriesAnalytics({
      period: this.state.period
    })
      .then(({ analytics }) => {
        if (!this.canvasRef.current) {
          return
        }
        const datasets = Object.keys(analytics.views).map(categoryId => {
          const category = categories.find(category => category.id == categoryId)
          return {
            label: category.name,
            fill: 'start',
            data: analytics['views'][categoryId],
            backgroundColor: 'transparent',
            borderColor: `#${category.color}`,
            pointBackgroundColor: `#${category.color}`,
            pointHoverBackgroundColor: `#${category.color}`,
            borderWidth: 1.5,
            pointRadius: 3,
            pointHoverRadius: 3
          }
        })
        if (this.chart) {
          this.chart.destroy()
        }
        let format = formattingMap[this.state.period] || 'D MMM'
        const now = moment()

        this.chart = new Chart(this.canvasRef.current, {
          type: 'line',
          data: {
            labels: analytics.timestamps
              .map(item => {
                const timeEntity = moment.unix(item).local()
                if (!this.state.period) {
                  const diff = Math.abs(now.diff(timeEntity, 'months'))
                  if (diff > 12) {
                    format = 'MMM Y'
                  } else {
                    format = 'D MMM'
                  }
                }
                return timeEntity.format(format)
              }),

            datasets
          },
          options: this.getChartOptions()
        })

        this.legendRef.current.innerHTML = this.chart.generateLegend()

        const meta = this.chart.getDatasetMeta(0)
        meta.data[0]._model.radius = 0
        meta.data[
          datasets[0].data.length - 1
        ]._model.radius = 0

        this.chart.render()
      })
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.period !== prevState.period) {
      this.fetchAndRender(this.state.categories)
    }
  }

  onChangePeriod = (period) => () => {
    this.setState({ period })
  }

  render() {
    const { title } = this.props

    return (
      <Card small className="col-12 p-0 categories-popularity">
        <CardHeader className="border-bottom card__header">
          <h5 className="m-0">{title}</h5>
        </CardHeader>

        <CardBody className="pt-0">
          <Row className="border-bottom py-2 bg-light">
            <Col sm="6" className="col d-flex mb-2 mb-sm-0">
              <ButtonGroup className="categories-popularity__period-selector">
                {
                  Object.keys(periodsMap).map((periodName, key) => {
                    return <Button
                      key={key}
                      theme="white"
                      active={this.state.period === periodsMap[periodName]}
                      onClick={this.onChangePeriod(periodsMap[periodName])}
                    >
                      {periodName}
                    </Button>
                  })
                }
              </ButtonGroup>
            </Col>
          </Row>

          <div ref={this.legendRef} />
          <div ref={this.chartWrapper} className="chart-wrapper">
            <div className="canvas-wrapper" style={{position: 'relative', display: 'block', height:'250px'}}>
              <canvas
                height="120"
                ref={this.canvasRef}
              />
            </div>
          </div>
        </CardBody>
      </Card>
    )
  }
}
