import React from 'react'

export default function Logo() {
  return <div className="logo">
    <img src="/images/logo.jpg" alt="QCards"></img>
  </div>
}
