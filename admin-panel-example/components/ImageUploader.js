import React, { PureComponent } from 'react'
import { Button, Modal, ModalBody, ModalHeader } from 'shards-react'
import { request } from '../services/api'
import ReactCrop from 'react-image-crop'

const MINIMUM_CROP_DIMENSION = 150
const MAXIMUM_CROP_DIMENSION = 300

export default class ImageUploader extends PureComponent {
  containerRef

  state = {
    crop: {
      x: 0,
      y: 0,
      height: 150,
      // width: 150,
      unit: 'px'
    },
    error: null,
  }

  allowedFileTypes = [
    'image/jpeg',
    'image/png',
  ]

  constructor(props) {
    super(props)
    this.containerRef = React.createRef()
  }

  toggle = () => {
  }

  onCropComplete = crop => {
    this.setState({ crop, error: null })
  }

  onImageLoaded = image => {
    this.imageRef = image
  };

  onCropChange = (crop, percentCrop) => {
    this.setState({ crop })
  }

  getCroppedImg(image, crop) {
    const canvas = document.createElement('canvas')
    const scaleX = image.naturalWidth / image.width
    const scaleY = image.naturalHeight / image.height
    let targetWidth = crop.width * scaleX
    let targetHeight = crop.height * scaleY
    // if (targetWidth > MAXIMUM_CROP_DIMENSION) {
    //   targetWidth = crop.width * MAXIMUM_CROP_DIMENSION / crop.width
    // }
    // if (targetHeight > MAXIMUM_CROP_DIMENSION) {
    //   targetHeight = crop.height * MAXIMUM_CROP_DIMENSION / crop.height
    // }
    canvas.width = targetWidth
    canvas.height = targetHeight
    const ctx = canvas.getContext('2d')

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      targetWidth,
      targetHeight
    )

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          reject(new Error('Unable to load an image'))
          return
        }
        window.URL.revokeObjectURL(this.state.imageUrl)
        resolve(blob)
      }, this.state.file.type)
    })
  }

  onClickToUpload = () => {
    this.getCroppedImg(this.imageRef, this.state.crop).then((blob) => {
      const data = new FormData()
      data.append('files', blob, this.state.file.name)
      request('/media', 'POST', data, false)
        .then(({ media }) => {
          const image = media[0]
          this.props.onUpload(image)
          this.setState({
            imageUrl: null,
            crop: {},
            error: null,
            file: null,
          })
        })
        .catch(error => {
          this.setState({ error: error.message })
        })
    })
  }

  onFilesSelect = (event) => {
    const file = event.target.files[0]
    this.setState({ error: null })
    if (!this.allowedFileTypes.includes(file.type)) {
      this.setState({ error: 'A format of the file is not supported. Please choose an image with one of format: JPEG, PNG' })
      return
    }
    this.setState({
      imageUrl: window.URL.createObjectURL(file),
      file: file,
    })
  }

  onCancel = () => {
    this.setState({
      imageUrl: null,
      error: null,
      file: null,
      crop: {},
    })
  }

  render() {
    const hasImage = Boolean(this.props.url)
    return <div className="text-center">
      <div
        className={'edit-user-details__avatar m-auto'
          + (!hasImage ? ' edit-user-details__avatar--no-image' : '')
        }
      >
        {this.props.url
          ? <img
            src={this.props.url}
            alt="Company Logo"
          />
          : <div className="user-avatar"></div>
        }
        <label className="edit-user-details__avatar__change">
          <i className="material-icons mr-1">&#xE439;</i>
          <input
            type="file"
            value={this.state.file ? void 0 : ''}
            accept={this.allowedFileTypes.join()}
            onChange={this.onFilesSelect}
            className="d-none"
          />
        </label>
        {this.state.imageUrl
          && <Modal centered open toggle={this.toggle}
            className="image-uploader"
          >
            <ModalHeader
              className="modal__header text-left ml-0"
            >
              {this.props.header}
            </ModalHeader>
            <ModalBody className="text-center">
              <div className="image-uploader__wrapper">
                <div ref={this.containerRef}>
                  <ReactCrop
                    src={this.state.imageUrl}
                    crop={this.state.crop}
                    onImageLoaded={this.onImageLoaded}
                    onComplete={this.onCropComplete}
                    onChange={this.onCropChange}
                    // maxWidth={MAXIMUM_CROP_DIMENSION}
                    // maxHeight={MAXIMUM_CROP_DIMENSION}
                    // minWidth={MINIMUM_CROP_DIMENSION}
                    // minHeight={MINIMUM_CROP_DIMENSION}
                  />
                </div>
              </div>
              {this.state.error
                && <span className="input__error">{this.state.error}</span>
              }
              <div className="modal__buttons">
                <Button
                  theme="light"
                  pill
                  className="mr-4"
                  type="button"
                  onClick={this.onCancel}
                >
                  Cancel
                </Button>
                <Button
                  pill
                  type="button"
                  onClick={this.onClickToUpload}
                >
                  Upload
                </Button>
              </div>
            </ModalBody>
          </Modal>
        }
      </div>
      {!this.state.imageUrl && this.state.error
        && <span className="input__error">{this.state.error}</span>
      }
    </div>
  }
}
