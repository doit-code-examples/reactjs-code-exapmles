import React, { PureComponent as Component } from 'react'
import {
  Container,
  Row,
  Card,
  CardHeader,
  CardBody,
  Col,
  FormInput,
  InputGroup,
  InputGroupAddon,
  Button,
  Alert
} from 'shards-react'
import PageTitle from '../../PageTitle'
import { list } from '../../../api/attribute';
import { list as valuesList, setBulk } from '../../../api/attributeValue';
import { length } from '../../Form/validators'
import ConfirmationModal, { Levels } from '../../ConfirmationModal';
import {
  setActionRequiredBeforeLeave,
  unsetActionRequiredBeforeLeave
} from '../../../actions/layout'
import { connect } from 'react-redux'
import PageError from '../../PageError';
import App from '../../../config/App';
import RoleCode from '../../../enums/RoleCode';

class ContentAttributes extends Component {
  state = {
    attributes: [],
    attributeValues: [],
    newValues: {},
    isChanged: false,
    isLoading: false,
    showRemoveConfirmationFor: null,
    error: null,
    pageError: null,
  }

  originalValues

  componentDidMount() {
    const isAuthorizedUserMarketingAdmin = this.props.auth.user
      && this.props.auth.user.userRoles.find(role => {
        return role.companyId === new App().getCompany().id && role.role.code === RoleCode.MARKETING_ADMIN
      })
    if (!isAuthorizedUserMarketingAdmin) {
      const error = new Error('Access denied')
      error.status = 403
      this.setState({ pageError: error })
    }

    list()
      .then(response => {
        this.setState({ attributes: response.attributes })
      })
      .catch(console.log)
    valuesList()
      .then(response => {
        const { attributeValues } = response
        this.originalValues = attributeValues
        this.setState({ attributeValues })
      })
      .catch((e) => {
      })
  }

  addAttributeValue = (attributeId) => () => {
    const value = this.state.newValues[attributeId].value
    let error = this.validateValue(value, attributeId)
    if (!error && this.state.attributeValues.find(attributeValue => attributeValue.attributeId === attributeId
      && attributeValue.value === value)
    ) {
      error = 'The value should be unique'
    }
    if (error) {
      this.setState(state => {
        return {
          newValues: {
            ...state.newValues,
            [attributeId]: {
              value,
              error: error
            }
          }
        }
      })
      return
    }
    if (!this.state.isChanged) {
      this.props.dispatch(setActionRequiredBeforeLeave())
    }
    this.setState(state => {
      return {
        isChanged: true,
        attributeValues: [
          ...state.attributeValues,
          {
            value: state.newValues[attributeId].value,
            attributeId
          }
        ],
        newValues: {
          ...state.newValues,
          [attributeId]: {
            value: '',
            error: ''
          }
        }
      }
    })
  }

  onDeleteAttributeValue = (attributeId, value) => event => {
    this.setState(state => {
      return {
        showRemoveConfirmationFor: {
          attributeId,
          value
        }
      }
    })
  }

  remove = () => {
    if (!this.state.isChanged) {
      this.props.dispatch(setActionRequiredBeforeLeave())
    }
    this.setState(state => {
      return {
        isChanged: true,
        showRemoveConfirmationFor: null,
        attributeValues: state.attributeValues.filter(attributeValue => {
          return !(attributeValue.attributeId === state.showRemoveConfirmationFor.attributeId
            && attributeValue.value === state.showRemoveConfirmationFor.value
          )
        })
      }
    })
  }

  onRemoveCancel = () => {
    this.setState({ showRemoveConfirmationFor: null })
  }

  onChangeNewValue = (attributeId) => event => {
    if (!this.state.isChanged) {
      this.props.dispatch(setActionRequiredBeforeLeave())
    }
    const value = event.target.value
    this.setState(state => {
      return {
        newValues: {
          ...state.newValues,
          [attributeId]: {
            value,
            error: ''
          }
        }
      }
    })
  }

  onChangeValue = (attributeId, valueId, keyOfChanged) => event => {
    if (!this.state.isChanged) {
      this.props.dispatch(setActionRequiredBeforeLeave())
    }
    const value = event.target.value
    this.setState(state => {
      return {
        isChanged: true,
        attributeValues: state.attributeValues.map((attributeValue, key) => {
          if (valueId && attributeValue.id === valueId
            || (!valueId
              && attributeValue.attributeId === attributeId
              && key === keyOfChanged
            )
          ) {
            return {
              ...attributeValue,
              value,
              error: ''
            }
          }
          return attributeValue
        })
      }
    })
  }

  validateValue(value, attributeId) {
    let maxLength = 255
    if (attributeId) {
      const attribute = this.state.attributes.find(item => item.id === attributeId)
      maxLength = attribute.maxLength
    }
    return length(1, maxLength)(value)
  }

  onSubmit = (event) => {
    event.preventDefault()
    let hasError = false
    const attributeValues = this.state.attributeValues.map((attributeValue, key) => {
      const { value, attributeId } = attributeValue
      const error = this.validateValue(value, attributeId)
      if (error) {
        hasError = true
        return {
          ...attributeValue,
          error
        }
      }
      if (hasError) {
        return attributeValue
      }
      if (this.state.attributeValues.find((otherAttributeValue, key2) => {
        return key !== key2
          && otherAttributeValue.attributeId === attributeValue.attributeId
          && otherAttributeValue.value === attributeValue.value
      })) {
        hasError = true
        return {
          ...attributeValue,
          error: 'The value should be unique'
        }
      }
      return attributeValue
    })

    if (hasError) {
      this.setState({
        attributeValues,
      })
      return
    }
    this.setState({ isLoading: true, error: null })
    setBulk(this.state.attributeValues)
      .then(({ attributeValues }) => {
        this.props.dispatch(unsetActionRequiredBeforeLeave())
        this.originalValues = attributeValues
        this.setState({
          attributeValues,
          isChanged: false,
          isLoading: false
        })
      })
      .catch(response => {
        this.setState({ isLoading: false })
        if (response.fields && response.fields.attributeValues) {
          const withErrorIndex = Object.keys(response.fields.attributeValues)[0]
          this.setState(state => {
            return {
              attributeValues: state.attributeValues.map((attributeValue, index) => {
                if (index == withErrorIndex) {
                  attributeValue = { ...attributeValue }
                  const field = Object.keys(response.fields.attributeValues[withErrorIndex])[0]
                  attributeValue.error = response.fields.attributeValues[withErrorIndex][field]
                }
                return attributeValue
              })
            }
          })
        } else {
          this.setState({ error: response.message })
        }
      })
  }

  onClickCancel = event => {
    this.props.dispatch(unsetActionRequiredBeforeLeave())
    this.setState({
      attributeValues: this.originalValues,
      isChanged: false
    })
  }

  onClickToHideError = () => {
    this.setState({ error: null })
  }

  render() {
    const { pageError } = this.state

    if (pageError) {
      return <PageError error={pageError} />
    }

    return <Container fluid className="main-content-container px-4">
      <Row noGutters className="page-header py-4">
        <PageTitle sm="4" title="Content Attributes" className="text-sm-left" />
      </Row>
      <Row>
        <Col className="content-attributes">
          <Card small className="mb-4">
            <CardHeader>
              <h5 className="m-0">Attributes</h5>
            </CardHeader>
            <CardBody>
              <Row>
                {this.state.error
                  && <Alert
                    theme="danger"
                    dismissible={this.onClickToHideError}
                  >
                    {this.state.error}
                  </Alert>
                }
                <form onSubmit={this.onSubmit}>
                  <div>
                    {this.state.attributes.map(attribute => {
                      const newValue = this.state.newValues[attribute.id]
                        || {
                          value: '',
                          error: ''
                        }
                      return <div key={attribute.id} className="content-attributes__section">
                        <div className="form-group">
                          <label htmlFor={`new-${attribute.name}`}>
                            {attribute.code !== 'audience' ? `${attribute.name}s` : attribute.name}
                          </label>
                          <div className="content-attributes__new-field">
                            <InputGroup className="ml-auto">
                              <FormInput
                                name={attribute.name}
                                id={`new-${attribute.name}`}
                                placeholder={'Add New ' + attribute.name}
                                value={newValue.value}
                                onChange={this.onChangeNewValue(attribute.id)}
                                invalid={!!newValue.error}
                              />

                              <InputGroupAddon type="append">
                                <Button
                                  theme="white"
                                  className="px-2 active"
                                  onClick={this.addAttributeValue(attribute.id)}
                                  disabled={newValue.value.length < 1}
                                >
                                  <i className="material-icons">add</i>
                                </Button>
                              </InputGroupAddon>

                            </InputGroup>
                            {newValue.error
                              && <span className="input__error">
                                {newValue.error}
                              </span>
                            }
                          </div>
                          {this.state.attributeValues
                            .map((value, index) => {
                              if (value.attributeId !== attribute.id) {
                                return
                              }
                              return <div
                                key={value.id || `${attribute.id}-${index}`}
                                className="content-attributes__value"
                              >
                                <InputGroup className="ml-auto">
                                  <FormInput
                                    value={value.value}
                                    onChange={this.onChangeValue(attribute.id, value.id, index)}
                                    invalid={!!value.error}
                                  />
                                  <InputGroupAddon type="append">
                                    <Button
                                      theme="white"
                                      className="px-2"
                                      onClick={this.onDeleteAttributeValue(attribute.id, value.value)}
                                    >
                                      <i className="material-icons">close</i>
                                    </Button>
                                  </InputGroupAddon>
                                </InputGroup>
                                {value.error
                                  && <span className="input__error">
                                    {value.error}
                                  </span>
                                }
                              </div>
                            })
                          }
                        </div>
                      </div>

                    })}
                  </div>
                  <Col className="action-buttons">
                    <Button
                      pill
                      theme="light"
                      type="button"
                      onClick={this.onClickCancel}
                      disabled={!this.state.isChanged}
                    >
                      Cancel
                    </Button>
                    <Button
                      pill
                      type="submit"
                      disabled={!this.state.isChanged || this.state.isLoading}
                    >
                      Save Changes
                    </Button>
                  </Col>
                </form>
              </Row>
            </CardBody>
          </Card>
        </Col>
      </Row>
      {this.state.showRemoveConfirmationFor
        && <ConfirmationModal
          onConfirm={this.remove}
          onCancel={this.onRemoveCancel}
          message="Are you sure you want to delete this Attribute?"
          confirmButtonText="Delete"
          level={Levels.CRITICAL}
        />
      }
    </Container>
  }

  componentWillUnmount() {
    this.props.dispatch(unsetActionRequiredBeforeLeave())
  }
}

export default connect(state => ({
  auth: state.auth,
}))(ContentAttributes)
