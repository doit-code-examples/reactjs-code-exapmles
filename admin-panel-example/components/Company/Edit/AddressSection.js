import React from 'react'
import { Col, FormGroup, FormInput, FormSelect, Row } from 'shards-react'

function getAddressFieldName(name, isBilling = false) {
  return `${isBilling ? 'address2' : 'address1'}.${name}`
}

export default function AddressSection(props) {
  const { getControlProps, isBilling, countries } = props
  const address1 = getControlProps(getAddressFieldName('address1', isBilling))
  const address2 = getControlProps(getAddressFieldName('address2', isBilling))
  const countryId = getControlProps(getAddressFieldName('countryId', isBilling))
  const region = getControlProps(getAddressFieldName('region', isBilling))
  const city = getControlProps(getAddressFieldName('city', isBilling))
  const postal = getControlProps(getAddressFieldName('postal', isBilling))

  return <Col className="card-section">
    <h5>{isBilling ? 'Billing ' : ''} Address</h5>
    <Row>
      <Col md='10'>
        <FormGroup>
          <span className="input__label">
            Street Address 1 <span className="required-mark">*</span>
          </span>
          <FormInput
            type="text"
            placeholder="Street Address 1"
            invalid={!!address1.error}
            onChange={address1.onChange}
            value={address1.value}
          />
          {address1.error
            && <span className="input__error">
              {address1.error}
            </span>
          }
        </FormGroup>
        <FormGroup>
          <span className="input__label">Street Address 2</span>
          <FormInput
            type="text"
            placeholder="Street Address 2"
            autoComplete='false'
            invalid={!!address2.error}
            onChange={address2.onChange}
            value={address2.value}
          />
          {address2.error
            && <span className="input__error">
              {address2.error}
            </span>
          }
        </FormGroup>
      </Col>
    </Row>

    <Row>
      <Col md='5'>
        <FormGroup>
          <span className="input__label">
            Country
            <span className="required-mark"> *</span>
          </span>
          <FormSelect
            autoComplete="country-name"
            invalid={!!countryId.error}
            onChange={(event) => {
              const { value } = event.target
              countryId.onChange(Number(value))
            }}
            value={countryId.value || 0}
          >
            <option value="0">Select</option>
            {countries.map(country => {
              return <option
                key={country.id}
                value={country.id}
              >
                {country.name}
              </option>
            })}
          </FormSelect>
          {countryId.error
            && <span className="input__error">
              {countryId.error}
            </span>
          }
        </FormGroup>
      </Col>
      <Col md='5'>
        <FormGroup>
          <span className="input__label">
            State <span className="required-mark">*</span>
          </span>
          <FormInput
            type="text"
            placeholder="State"
            invalid={!!region.error}
            onChange={region.onChange}
            value={region.value}
          />
          {region.error
            && <span className="input__error">
              {region.error}
            </span>
          }
        </FormGroup>
      </Col>
    </Row>
    <Row>
      <Col md='5'>
        <FormGroup>
          <span className="input__label">
            City <span className="required-mark">*</span>
          </span>
          <FormInput
            type="text"
            placeholder="City"
            invalid={!!city.error}
            onChange={city.onChange}
            value={city.value}
          />
          {city.error
            && <span className="input__error">
              {city.error}
            </span>
          }
        </FormGroup>
      </Col>
      <Col md='5'>
        <FormGroup>
          <span className="input__label">
            Postal Code <span className="required-mark">*</span>
          </span>
          <FormInput
            type="text"
            placeholder="Postal Code"
            invalid={!!postal.error}
            onChange={postal.onChange}
            value={postal.value}
          />
          {postal.error
            && <span className="input__error">
              {postal.error}
            </span>
          }
        </FormGroup>
      </Col>
    </Row>
  </Col>
}
