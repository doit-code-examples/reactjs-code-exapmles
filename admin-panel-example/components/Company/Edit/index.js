import omit from 'lodash/omit'
import moment from 'moment'
import React, { PureComponent } from 'react'
import ClickOutside from 'react-click-outside'
import { CirclePicker } from 'react-color'
import { connect } from 'react-redux'
import { Alert, Button, Card, CardBody, Col, Container, Form, FormCheckbox, FormGroup, FormInput, FormTextarea, Row } from 'shards-react'
import { companyAdd } from "../../../actions/company"
import { setActionRequiredBeforeLeave, unsetActionRequiredBeforeLeave } from '../../../actions/layout'
import { remove, update } from '../../../api/addresses'
import { addAddress, get, update as updateCompany } from '../../../api/company'
import { list } from '../../../api/country'
import { pause, resume } from '../../../api/subscription'
import App from '../../../config/App'
import Company from '../../../enums/Company'
import RoleCode from '../../../enums/RoleCode'
import { presentType } from '../../../services/subscription'
import FormWrapper from '../../Form'
import { length, required } from '../../Form/validators'
import ImageUploader from '../../ImageUploader'
import PageError from '../../PageError'
import PageTitle from '../../PageTitle'
import SuccessMessage from '../../SuccessMessage'
import AddressSection from './AddressSection'

const COLORS = [
  '#f44336',
  '#e91e63',
  '#9c27b0',
  '#673ab7',
  '#3f51b5',
  `#${Company.DEFAULT_COLOR}`,
  '#03a9f4',
  '#00bcd4',
  '#009688',
  '#4caf50',
  '#8bc34a',
  '#cddc39',
  '#ffeb3b',
  '#ffc107',
  '#ff9800',
  '#ff5722',
  '#795548',
  '#607d8b'
]

class Edit extends PureComponent {
  state = {
    countries: [],
    isChanged: false,
    fields: {},
    isSaved: false,
    isLoading: false,
    showColorPicker: false,
    error: null,
  }

  originalValues = {
    isChanged: false,
    isBillingAddressDiffer: false,
    address1: {
      id: null,
      type: 'billing',
    },
    address2: {
      id: null,
      type: 'billing'
    },
  }

  company

  constructor(props) {
    super(props)
    this.company = new App().getCompany()
    this.state = {
      ...this.state,
      ...this.originalValues,
      fields: this.getFields()
    }
  }

  getFields() {
    return {
      'company.name': {
        validator: [required, length(1, 255)]
      },
      'company.description': {
      },
      'company.color': {
      },
      'address1.address1': {
        validator: [required, length(1, 255)]
      },
      'address1.address2': {
      },
      'address1.city': {
        validator: [required, length(0, 255)]
      },
      'address1.countryId': {
        validator: [required]
      },
      'address1.postal': {
        validator: [required]
      },
      'address1.region': {
        validator: [required]
      },
      'address2.address1': {
        validator: [this.billingAddressRequiredRule, length(1, 255)]
      },
      'address2.address2': {
      },
      'address2.city': {
        validator: [this.billingAddressRequiredRule, length(0, 255)]
      },
      'address2.countryId': {
        validator: [this.billingAddressRequiredRule]
      },
      'address2.postal': {
        validator: [this.billingAddressRequiredRule]
      },
      'address2.region': {
        validator: [this.billingAddressRequiredRule]
      },
    }
  }

  billingAddressRequiredRule = (value) => {
    return this.state.isBillingAddressDiffer
      ? required(value)
      : false
  }

  componentDidMount() {
    const isAuthorizedUserMarketingAdmin = this.props.auth.user
      && this.props.auth.user.userRoles.find(role => {
        return role.companyId === new App().getCompany().id && role.role.code === RoleCode.MARKETING_ADMIN
      })
    if (!isAuthorizedUserMarketingAdmin) {
      const error = new Error('Access denied')
      error.status = 403
      this.setState({ pageError: error })
    }
    this.setState({ isLoading: true })
    get(this.company.id, {
      include: 'addresses,logo,subscription'
    })
      .then(({ company }) => {
        this.props.dispatch(companyAdd(company));
        const fields = { ...this.state.fields }
        fields['company.name'] = {
          ...this.state.fields['company.name'],
          value: company.name
        }
        fields['company.description'] = {
          ...this.state.fields['company.description'],
          value: company.description
        }
        fields['company.color'] = {
          ...this.state.fields['company.color'],
          value: company.color
        }
        const state = {
          ...this.state,
        }
        state.subscription = company.subscription
        if (company.logo) {
          state.logo = company.logoMediaId
          state.logoUrl = company.logo.url
        }

        let billingAddress
        let generalAddress
        if (company.addresses.length > 1) {
          billingAddress = company.addresses.find(address => address.type === 'billing')
          const notBillingAddress = company.addresses.find(address => address.type !== 'billing')
          state.isBillingAddressDiffer = notBillingAddress && billingAddress ? true : false
          generalAddress = notBillingAddress || billingAddress
          if (billingAddress) {
            state.address2 = {
              ...state.address2,
              id: billingAddress.id,
              type: billingAddress.type
            }
          }
        } else if (company.addresses.length <= 1) {
          generalAddress = company.addresses[0]
        }

        if (generalAddress) {
          state.address1 = {
            ...state.address1,
            id: generalAddress.id,
            type: generalAddress.type
          }
        }

        state.isLoading = false
        state.fields = Object.keys(fields).reduce((acc, item) => {
          if (item.startsWith('address')) {
            const pieces = item.split('.')
            const type = pieces[0]
            let address
            if (type === 'address1') {
              address = generalAddress
            } else if (type === 'address2') {
              address = billingAddress
            }
            const name = pieces[1]
            if (address && address.hasOwnProperty(name)) {
              acc[item] = {
                ...this.state.fields[item],
                value: address[name]
              }
            }
          }
          return acc
        }, fields)
        this.originalValues = {
          ...this.originalValues,
          ...omit(state, 'countries')
        }
        this.setState(state)
      })
      .catch(() => {
        this.setState({ isLoading: false })
      })

    list()
      .then(({ countries }) => {
        this.setState({ countries })
      })
  }

  toggleBillingAddressDiffer = (event) => {
    this.setState(state => {
      const isBillingAddressDiffer = !state.isBillingAddressDiffer
      return {
        isChanged: true,
        isBillingAddressDiffer,
        address1: {
          ...state.address1,
          type: isBillingAddressDiffer ? null : 'billing'
        },
        address2: {
          ...state.address2,
          type: isBillingAddressDiffer ? 'billing' : null
        }
      }
    })
    this.props.dispatch(unsetActionRequiredBeforeLeave())
  }

  toggleSubscriptionState = () => {
    const { subscription } = this.state
    const isActive = subscription.isActive
    if (isActive) {
      pause(subscription.id)
        .then(() => {
          this.setState({
            subscription: {
              ...subscription,
              isActive: false
            }
          })
        })
    } else {
      resume(subscription.id)
        .then(() => {
          this.setState({
            subscription: {
              ...subscription,
              isActive: true
            }
          })
        })
    }
  }

  onSubmit = (data) => {
    if (!data) {
      return
    }

    this.setState({ isLoading: true, error: null })
    const promises = []
    const newCompanyData = {
      name: data['company.name'],
      description: data['company.description'],
      logoMediaId: this.state.logo || null,
      color: data['company.color'] === Company.DEFAULT_COLOR || !data['company.color']
        ? null
        : data['company.color']
    }
    promises.push(
      updateCompany(
        this.company.id,
        newCompanyData
      )
        .then(() => {
          this.props.dispatch(companyAdd(newCompanyData))
        })
        .catch(error => {
          if (error.fields) {
            this.setState({
              fields: this.fillWithErrorsCompositeFields(error.fields, this.state.fields, 'company')
            })
          } else {
            this.setState({ error: error.message })
          }
          return Promise.reject(error)
        })
    )

    const address1 = {
      address1: data['address1.address1'],
      address2: data['address1.address2'] || null,
      city: data['address1.city'],
      countryId: data['address1.countryId'],
      postal: data['address1.postal'],
      region: data['address1.region'],
      type: this.state.address1.type
    }
    if (this.state.address1.id) {
      promises.push(
        update(this.state.address1.id, address1)
          .catch(error => {
            if (error.fields) {
              this.setState({
                fields: this.fillWithErrorsCompositeFields(error.fields, this.state.fields, 'address1')
              })
            } else {
              this.setState({ error: error.message })
            }
            return Promise.reject(error)
          })
      )
    } else {
      promises.push(
        addAddress(address1)
          .then(({ address }) => {
            return this.setState({
              address1: {
                ...this.state.address1,
                id: address.id,
                type: address.type
              }
            })
          })
          .catch(error => {
            if (error.fields) {
              this.setState({
                fields: this.fillWithErrorsCompositeFields(error.fields, this.state.fields, 'address1')
              })
            } else {
              this.setState({ error: error.message })
            }
            return Promise.reject(error)
          })
      )
    }
    if (this.state.address2.id && this.state.address1.type === 'billing') {
      promises.push(
        remove(this.state.address2.id)
          .catch(error => {
            if (error.status === 404) {
              return
            }
            return Promise.reject(error)
          })
      )
    }
    if (this.state.isBillingAddressDiffer) {
      const address2 = {
        address1: data['address2.address1'],
        address2: data['address2.address2'] || null,
        city: data['address2.city'],
        countryId: data['address2.countryId'],
        postal: data['address2.postal'],
        region: data['address2.region'],
        type: this.state.address2.type
      }
      if (this.state.address2.id) {
        promises.push(
          update(this.state.address2.id, address2)
            .catch(error => {
              if (error.fields) {
                this.setState({
                  fields: this.fillWithErrorsCompositeFields(error.fields, this.state.fields, 'address2')
                })
              } else {
                this.setState({ error: error.message })
              }
              return Promise.reject(error)
            })
        )
      } else {
        promises.push(
          addAddress(address2)
            .then(({ address }) => {
              return this.setState({
                address2: {
                  ...this.state.address2,
                  id: address.id,
                  type: address.type,
                }
              })
            })
            .catch(error => {
              if (error.fields) {
                this.setState({
                  fields: this.fillWithErrorsCompositeFields(error.fields, this.state.fields, 'address2')
                })
              } else {
                this.setState({ error: error.message })
              }
              return Promise.reject(error)
            })
        )
      }
    }

    Promise.all(promises)
      .then(() => {
        this.setState({ isSaved: true, isLoading: false })
        this.props.dispatch(unsetActionRequiredBeforeLeave())
      })
      .catch((error) => {
        this.setState({ isLoading: false })
      })
  }

  goBack = () => {
    this.props.history.push('/company')
  }

  onUpdateField = (changedField, value) => {
    this.setState({
      fields: Object.keys(this.state.fields).reduce((acc, field) => {
        if (changedField === field) {
          acc[field] = {
            ...omit(this.state.fields[field], 'error'),
            value
          }
          return acc
        }
        acc[field] = this.state.fields[field]
        return acc
      }, {})
    })
    if (!this.state.isChanged) {
      this.setState({ isChanged: true })
      this.props.dispatch(setActionRequiredBeforeLeave())
    }
  }

  onClickCancel = () => {
    this.setState({ ...this.originalValues, isChanged: false })
    this.props.dispatch(unsetActionRequiredBeforeLeave())
  }

  fillWithErrorsCompositeFields(errors, target, prefix) {
    return Object.keys(target).reduce((acc, field) => {
      if (field.startsWith(prefix)) {
        const fieldName = field.split('.')[1]
        if (errors.hasOwnProperty(fieldName)) {
          acc[field] = {
            ...target[field],
            error: errors[fieldName]
          }
          return acc
        }
      }
      acc[field] = target[field]
      return acc
    }, {})
  }

  updateLogo = (media) => {
    this.setState({
      logo: media.id,
      logoUrl: media.url
    })
    if (!this.state.isChanged) {
      this.setState({ isChanged: true })
      this.props.dispatch(setActionRequiredBeforeLeave())
    }
  }

  deleteLogo = () => {
    this.setState({
      logo: null,
      logoUrl: null
    })
    if (!this.state.isChanged) {
      this.setState({ isChanged: true })
      this.props.dispatch(setActionRequiredBeforeLeave())
    }
  }

  onCancelChangingLogo = () => {
  }

  toggleShowingColorPicker = () => {
    this.setState({
      showColorPicker: !this.state.showColorPicker
    })
  }

  renderGeneralInfoBlock(getControlProps) {
    const nameProps = getControlProps('company.name')
    const descriptionProps = getControlProps('company.description')
    const colorProps = getControlProps('company.color')
    return <div>
      <h5>General Info</h5>
      <Row>
        <Col md="9">
          <Row>
            <Col md="6">
              <FormGroup>
                <div>
                  <label htmlFor="company-name">Company Name</label>
                  <span className="required-mark"> *</span>
                </div>
                <FormInput
                  type="text"
                  id="company-name"
                  placeholder="Company Name"
                  invalid={!!nameProps.error}
                  onChange={nameProps.onChange}
                  value={nameProps.value}
                />
                {nameProps.error
                  && <span className="input__error">
                    {nameProps.error}
                  </span>
                }
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <label htmlFor="description">Description</label>
                <FormTextarea
                  rows="5"
                  id="description"
                  placeholder="Write something good about your company"
                  invalid={!!descriptionProps.error}
                  onChange={descriptionProps.onChange}
                  value={descriptionProps.value}
                />
                {descriptionProps.error
                  && <span className="input__error">
                    {descriptionProps.error}
                  </span>
                }
              </FormGroup>
              <span
                className="input__label"
              >
                Company Color
              </span>
              <div className="color-selector">
                <span
                  className="color-selector__value"
                  onClick={this.toggleShowingColorPicker}
                  style={{
                    backgroundColor: `#${colorProps.value || Company.DEFAULT_COLOR }`
                  }}
                />
                {this.state.showColorPicker
                  && <ClickOutside
                    onClickOutside={this.toggleShowingColorPicker}
                  ><div className="color-selector__picker">
                      <CirclePicker
                        colors={COLORS}
                        onChangeComplete={(color) => {
                          colorProps.onChange(color.hex.substr(1).toLocaleUpperCase())
                          this.setState({ showColorPicker: false })
                        }}
                        color={`#${colorProps.value || Company.DEFAULT_COLOR}`}
                      />
                    </div>
                  </ClickOutside>
                }
              </div>
            </Col>
          </Row>
        </Col>
        <Col md="3">
          <FormGroup>
            <label
              className="text-center w-100 mb-4"
            >
              Company Logo
            </label>
            <ImageUploader
              header="Crop your company logo"
              url={this.state.logoUrl}
              onUpload={this.updateLogo}
              onCancel={this.onCancelChangingLogo}
            />
            {this.state.logo
              && <Button
                size="sm"
                type="button"
                theme="white"
                onClick={this.deleteLogo}
                className="d-table mx-auto mt-4"
              >
                Delete Logo
              </Button>
            }
          </FormGroup>
        </Col>
      </Row>
    </div>
  }

  onClickToHideError = () => {
    this.setState({ error: null })
  }

  render() {
    const { subscription, pageError } = this.state

    if (pageError) {
      return <PageError error={pageError} />
    }

    return (
      <Container fluid className="main-content-container px-4">
        <Row noGutters className="page-header py-4">
          <PageTitle title="Edit Mode" subtitle="Company" className="text-sm-left mb-3" />
        </Row>
        {this.state.isSaved
          ? <Row>
            <Col>
              <SuccessMessage
                message="The changes successfully saved"
                onButtonClick={this.goBack}
              />
            </Col>
          </Row>
          : <Row>
            <Col xs="12" sm="12" md='12' lg='8'>
              <Card small className="edit-company">
                <CardBody>
                  {this.state.error
                    && <Alert
                      theme="danger"
                      dismissible={this.onClickToHideError}
                    >
                      {this.state.error}
                    </Alert>
                  }
                  <FormWrapper
                    fields={this.state.fields}
                    onSubmit={this.onSubmit}
                    onUpdateField={this.onUpdateField}
                    render={(getControlProps, onSubmit) => {
                      return <Form onSubmit={onSubmit}>
                        {this.renderGeneralInfoBlock(getControlProps)}
                        <Row className="border-top">
                          <AddressSection
                            getControlProps={getControlProps}
                            countries={this.state.countries}
                          />
                        </Row>
                        <Row>
                          <Col md='10'>
                            <FormCheckbox
                              id='billing-checkbox'
                              onChange={this.toggleBillingAddressDiffer}
                              checked={this.state.isBillingAddressDiffer}
                            >
                              Customer&apos;s billing address is different from general address
                            </FormCheckbox>
                          </Col>
                        </Row>

                        {this.state.isBillingAddressDiffer
                          && <Row className="border-top">
                            <AddressSection
                              getControlProps={getControlProps}
                              isBilling
                              countries={this.state.countries}
                            />
                          </Row>
                        }
                        {subscription
                          && <Row className="border-top">
                            <Col className="card-section">
                              <h5>Subscription Type</h5>
                              <Row>
                                <Col md='10'>
                                  <FormGroup>
                                    <span className="input__label">Subscription</span>
                                    <div className="field__value">{presentType(subscription.type)}</div>
                                  </FormGroup>
                                  <FormGroup>
                                    <span className="input__label">Price</span>
                                    <div className="field__value">{subscription.price} $</div>
                                  </FormGroup>
                                </Col>
                              </Row>
                              <Row>
                                <Col md="6">
                                  <span className="input__label">Start Date</span>
                                  <div className="field__value">{moment(subscription.startedAt).local().format('MMMM DD, Y')}</div>
                                </Col>
                                <Col md="6">
                                  <span className="input__label">End Date</span>
                                  {subscription.endedAt
                                    && <div className="field__value">{moment(subscription.endedAt).local().format('MMMM DD, Y')}</div>
                                  }
                                </Col>
                              </Row>
                              <Row>
                                <Col md='10'>

                                  <div
                                    className="toggle-subscription-state pointer"
                                    onClick={this.toggleSubscriptionState}
                                  >
                                    <span className='pause-button'>
                                      <i className="material-icons">
                                        {subscription.isActive ? 'pause_circle_filled' : 'play_circle_filled'}
                                      </i>
                                    </span>
                                    <span className='pause-button-text'>{subscription.isActive ? 'Pause' : 'Resume'} Subscription</span>
                                  </div>
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                        }
                        <div className="action-buttons">
                          <Button
                            pill
                            theme="light"
                            type="button"
                            onClick={this.onClickCancel}
                            disabled={!this.state.isChanged || this.state.isLoading}
                          >
                            Cancel
                          </Button>
                          <Button
                            disabled={!this.state.isChanged || this.state.isLoading}
                            theme="accent"
                            pill
                            type="submit"
                          >
                            Save Changes
                          </Button>
                        </div>
                      </Form>
                    }}
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
        }
      </Container>
    )
  }

  componentWillUnmount() {
    this.props.dispatch(unsetActionRequiredBeforeLeave())
  }
}

export default connect(state => ({
  auth: state.auth
}))(Edit)
