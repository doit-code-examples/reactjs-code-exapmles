import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import {
  Row,
  Container,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
} from 'shards-react'
import PageTitle from '../PageTitle'
import { get, getSubscription } from '../../api/company';
import App from '../../config/App'
import { Link } from 'react-router-dom'
import moment from 'moment'
import { list as getCountries } from '../../api/country';
import { list as getAttributes } from '../../api/attribute';
import { list as getAttributeValues } from '../../api/attributeValue';
import RoleCode from '../../enums/RoleCode'
import CompanyEnum from '../../enums/Company'
import { presentType } from '../../services/subscription'
import {companyAdd} from "../../actions/company";

class Company extends PureComponent {
  state = {
    company: null,
    subscription: null,
    attributeValues: [],
    attributes: [],
    countries: []
  }

  company

  constructor(props) {
    super(props)
    this.company = new App().getCompany()
  }

  componentDidMount() {
    const isAuthorizedUserMarketingAdmin = this.props.auth.user
      && this.props.auth.user.userRoles.find(role => {
        return role.companyId === new App().getCompany().id && role.role.code === RoleCode.MARKETING_ADMIN
      })
    let relations = [
      'marketingAdmin',
      'logo'
    ]
    if (isAuthorizedUserMarketingAdmin) {
      relations = relations.concat(['addresses', 'admin'])
    }
    get(this.company.id, {
      include: relations.join()
    })
      .then(({ company }) => {
        const state = {
          company,
        }
        this.props.dispatch(companyAdd(company));
        if (isAuthorizedUserMarketingAdmin) {
          const billingAddress = company.addresses.find(address => address.type === 'billing')
          if (billingAddress && company.addresses.length > 1) {
            const generalAddress = company.addresses.find(address => address.type !== 'billing')
            state.address1 = generalAddress || company.addresses.find(address => address.id !== billingAddress.id)
            state.address2 = billingAddress
            state.isBillingAddressDiffer = generalAddress && billingAddress ? true : false
          }
          if (company.addresses.length <= 1) {
            state.address1 = company.addresses[0]
          }
        }
        this.setState(state)
      })
      .catch(() => { })

    if (isAuthorizedUserMarketingAdmin) {
      getCountries()
        .then(({ countries }) => {
          this.setState({ countries })
        })
        .catch(() => { })
      if (this.company.subscriptionId) {
        getSubscription(this.company.subscriptionId)
          .then(({ subscription }) => {
            this.setState({ subscription })
          })
          .catch(() => {})
      }
      getAttributes()
        .then(response => {
          this.setState({ attributes: response.attributes })
        })
        .catch(() => { })
      getAttributeValues()
        .then(response => {
          const { attributeValues } = response
          this.originalValues = attributeValues
          this.setState({ attributeValues })
        })
        .catch((e) => {
        })
    }
  }

  render() {
    const isAuthorizedUserMarketingAdmin = this.props.auth.user
      && this.props.auth.user.userRoles.find(role => {
        return role.companyId === new App().getCompany().id && role.role.code === RoleCode.MARKETING_ADMIN
      })
    const { company, subscription, countries, address1, address2 } = this.state
    const companyUrl = company && company.host ? `https://${company.host}` : null
    let usersCount
    let availableSlots
    if (company) {
      usersCount = company.marketersCount + company.salespersonsCount
      if (company.maxUsers !== null) {
        availableSlots = usersCount >= company.maxUsers
          ? 0
          : company.maxUsers - usersCount
      }
    }

    const countryAddress1 = address1
      ? countries.find(country => country.id === address1.countryId)
      : null
    const countryAddress2 = address2
      ? countries.find(country => country.id === address2.countryId)
      : null

    return <Container fluid className="main-content-container px-4 company-details">
      <Row noGutters className="page-header py-4">
        <PageTitle sm="4" title="Company" className="text-sm-left" />
      </Row>
      {company
        && <Row className="company-details">
          <Col lg="7" md="12" className="mb-4">
            <Card small>
              <CardHeader className="p-0 user-details__header">
                <div className="user-details__avatar-wrapper">
                  <div className="user-details__avatar">
                    {company.logoMediaId
                      && <img
                        src={company.logo.url}
                        alt="Company Logo"
                      />
                    }
                  </div>
                </div>
              </CardHeader>
              <CardBody>
                <Row className="resource-details__headline mb-3">
                  <Col>
                    <h5>{company.name}</h5>
                  </Col>
                  {isAuthorizedUserMarketingAdmin
                    && <Col className="resource-details__edit-button">
                      <Link to='/company/edit' className="simple-link">
                        <i
                          className="material-icons"
                        >
                          edit
                        </i>
                      </Link>
                    </Col>
                  }
                </Row>
                <Row>
                  {company.marketingAdmin
                  && <Col xs={12} sm={6}>
                    <span className="input__label">Company Administrator</span>
                    <div className="field__value">{company.marketingAdmin.firstName} {company.marketingAdmin.lastName}</div>
                  </Col>
                  }
                  {isAuthorizedUserMarketingAdmin
                    && company.admin
                    && <Col xs={12} sm={6} className="field">
                      <span className="input__label">VI Admin</span>
                      <div className="field__value">{company.admin.firstName} {company.admin.lastName}</div>
                    </Col>
                  }
                </Row>
                <Row>
                  <Col xs={12} sm={6}>
                    <span className="input__label">Amount of Company Users</span>
                    <p className="field__value">{usersCount}</p>
                  </Col>
                  <Col xs={12} sm={6}>
                    <span className="input__label">Available Slots</span>
                    <p className="field__value">
                      {isNaN(availableSlots) ? 'Unlimited' : availableSlots}
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12} sm={6}>
                    <span className="input__label">Company Color</span>
                    <div className="color-selector">
                      <span
                        className="color-selector__value"
                        style={{
                          backgroundColor: `#${company.color || CompanyEnum.DEFAULT_COLOR}`
                        }}
                      />
                    </div>
                  </Col>
                  <Col xs={12} sm={6}>
                    <span className="input__label">Company URL</span>
                    <p className="field__value">
                      <a className="simple-link" href={companyUrl}>{companyUrl}</a>
                    </p>
                  </Col>
                </Row>
                {isAuthorizedUserMarketingAdmin
                  && address1
                  && countries.length > 0
                  && <Row className="border-top">
                    <Col className="card-section">
                      <h5>Address</h5>
                      <Row>
                        <Col md="6">
                          <div>
                            <span className="input__label">Street Address 1</span>
                            <div className="field__value">{address1.address1}</div>
                          </div>
                        </Col>
                        <Col md="6">
                          <div>
                            <span className="input__label">Street Address 2</span>
                            <div className="field__value">{address1.address2}</div>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col md="6">
                          <span className="input__label">Country</span>
                          <div className="field__value">
                            {countryAddress1
                              ? countryAddress1.name
                              : null
                            }
                          </div>
                        </Col>
                        <Col md="6">
                          <span className="input__label">State</span>
                          <div className="field__value">{address1.region}</div>
                        </Col>
                      </Row>
                      <Row>
                        <Col md="6">
                          <span className="input__label">City</span>
                          <div className="field__value">{address1.city}</div>
                        </Col>
                        <Col md="6">
                          <span className="input__label">Postal Code</span>
                          <div className="field__value">{address1.postal}</div>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                }

                {isAuthorizedUserMarketingAdmin
                  && address2
                  && countries.length > 0
                  && <Row className="border-top">
                    <Col className="card-section">
                      <h5>Billing Address</h5>
                      <Row>
                        <Col md="6">
                          <div>
                            <span className="input__label">Street Address 1</span>
                            <div className="field__value">{address2.address1}</div>
                          </div>
                        </Col>
                        <Col md="6">
                          <div>
                            <span className="input__label">Street Address 2</span>
                            <div className="field__value">{address2.address2}</div>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        {address2.countryId && countries.length > 0
                          && <Col md="6">
                            <span className="input__label">Country</span>
                            <div className="field__value">
                              {countryAddress2
                                ? countryAddress2.name
                                : null
                              }
                            </div>
                          </Col>
                        }

                        <Col md="6">
                          <span className="input__label">State</span>
                          <div className="field__value">{address2.region}</div>
                        </Col>
                      </Row>
                      <Row>
                        <Col md="6">
                          <span className="input__label">City</span>
                          <div className="field__value">{address2.city}</div>
                        </Col>
                        <Col md="6">
                          <span className="input__label">Postal Code</span>
                          <div className="field__value">{address2.postal}</div>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                }
                {isAuthorizedUserMarketingAdmin
                  && subscription
                  && <Row className="border-top">
                    <Col className="card-section">
                      <h5>Subscription Type</h5>
                      <Row>
                        <Col md='10'>
                          <FormGroup>
                            <span className="input__label">Subscription</span>
                            <div className="field__value">{presentType(subscription.type)}</div>
                          </FormGroup>
                          <FormGroup>
                            <span className="input__label">Price</span>
                            <div className="field__value">{subscription.price} $</div>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col md="6">
                          <span className="input__label">Start Date</span>
                          <div className="field__value">{moment(subscription.startedAt).local().format('MMMM DD, Y')}</div>
                        </Col>
                        <Col md="6">
                          <span className="input__label">End Date</span>
                          {subscription.endedAt
                            && <div className="field__value">{moment(subscription.endedAt).local().format('MMMM DD, Y')}</div>
                          }
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                }
                {company.description
                  && <Row className="border-top">
                    <Col className="card-section">
                      <h5>Description</h5>
                      <Row>
                        <Col className="company-details__description">
                          {company.description}
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                }
              </CardBody>
            </Card>
          </Col>
          {isAuthorizedUserMarketingAdmin
            && this.state.attributes.length > 0
            && <Col lg="5" md="12" className="mb-4">
              <Card small>
                <CardHeader>
                  <Row className="resource-details__headline">
                    <Col>
                      <h5 className="m-0">Attributes</h5>
                    </Col>
                    <Col className="resource-details__edit-button">
                      <Link to='/company/attributes' className="simple-link">
                        <i
                          className="material-icons"
                        >
                          edit
                        </i>
                      </Link>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Row className="attributes-block">
                    <Col className="p-0">
                      {this.state.attributes.map(attribute => {
                        const values = this.state.attributeValues.filter(item => item.attributeId === attribute.id)
                        return <ul key={attribute.id} className="attributes-block__item">
                          <span className="input__label">{attribute.code !== 'audience' ? `${attribute.name}s` : attribute.name}</span>
                          {values.map((value, index) => {
                            return <li key={value.id || `${attribute.id}-${index}`}>
                              {value.value}
                            </li>
                          })}
                        </ul>
                      })}
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          }

        </Row>
      }
    </Container>
  }
}

export default connect(state => ({
  auth: state.auth
}))(Company)
