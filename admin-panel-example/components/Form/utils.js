export function fillWithErrors(source, target, defaultField) {
  let isErrorFilled = false
  let result = Object.keys(target).reduce((acc, field) => {
    if (source.hasOwnProperty(field)) {
      acc[field] = {
        ...target[field],
        error: source[field]
      }
      isErrorFilled = true
    } else {
      acc[field] = target[field]
    }
    return acc
  }, {})
  if (defaultField && !isErrorFilled) {
    result = {
      ...result,
      [defaultField]: {
        ...result[defaultField],
        error: Object.values(source)[0]
      }
    }
  }
  return result
}
