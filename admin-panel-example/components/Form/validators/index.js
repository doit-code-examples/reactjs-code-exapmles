import { string } from 'yup'
import { isFirstName, isLastName } from '../../../utils/validator';

export function required(value) {
  const error = 'This field is required'
  if (typeof value === 'string'
    && value.trim().length < 1
  ) {
    return error
  }
  return value ? false : error
}

export function length(min, max = null) {
  return value => {
    if (!value) {
      return false
    }
    if (typeof value === 'string') {
      value = value.trim()
    }
    const stringValidator = string()
    if (!stringValidator.min(min).isValidSync(value)) {
      return `The value must have at least ${min} characters`
    }
    if (max && !stringValidator.max(max).isValidSync(value)) {
      return `The value must have at most ${max} characters`
    }
    return false
  }
}

export function email(value) {
  return string()
    .email()
    .isValidSync(value)
    ? false
    : 'The value should be a valid email'
}

export function firstName(value) {
  return isFirstName(value)
    ? false
    : 'Only letters and apostrophe are allowed here'
}

export function lastName(value) {
  return isLastName(value)
    ? false
    : 'Only letters, whitespace, apostrophe, hyphen are allowed here'
}
