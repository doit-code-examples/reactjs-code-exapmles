import React, { Component } from 'react'
import get from 'lodash/get'
import isEqual from 'lodash/isEqual'

export default class Form extends Component {
  constructor(props) {
    super(props)

    this.state = {
      shouldValidate: this.props.validateOnChange || false,
      ...this.assignControlValues(this.props.fields)
    }
  }

  componentWillReceiveProps(newProps) {
    const newFieldsNames = Object.keys(newProps.fields)
    const fields = newFieldsNames.reduce((acc, fieldName) => {
      const oldField = this.props.fields[fieldName]
      let field
      if (oldField) {
        let isChanged = false
        if (!isEqual(oldField.value, newProps.fields[fieldName].value)) {
          isChanged = true
        }

        field = isChanged
          ? newProps.fields[fieldName]
          : {
            ...newProps.fields[fieldName],
            value: this.state.controlValues[fieldName],
            error: this.state.controlValidationErrors[fieldName]
          }
      } else {
        field = newProps.fields[fieldName]
      }

      return { ...acc, [fieldName]: field }
    }, {})
    this.setState({
      ...this.assignControlValues(fields)
    })
  }

  assignControlValues(fields) {
    const controlValues = {}
    const controlValidationErrors = {}

    Object.keys(fields).forEach(controlName => {
      const controlConfig = fields[controlName]
      controlValues[controlName] =
        controlConfig.value || controlConfig.value === false
          ? controlConfig.value
          : ''
      controlValidationErrors[controlName] = controlConfig.error || ''
    })

    return {
      controlValues,
      controlValidationErrors
    }
  }

  getControlProps = controlName => {
    const control = this.props.fields[controlName]

    if (typeof control == 'undefined') {
      throw new Error(
        `Tried to get properties from the not existed field ${controlName}`
      )
    }

    return {
      ...control,
      name: controlName,
      onChange: this.onControlChange(controlName),
      error: control.error || this.state.controlValidationErrors[controlName],
      value: this.state.controlValues[controlName],
      onError: this.onError(controlName)
    }
  }

  onError = controlName => error => {
    const controlValidationErrors = { ...this.state.controlValidationErrors }
    controlValidationErrors[controlName] = error
    this.setState({ controlValidationErrors })
  }

  onControlChange = name => (event, data = null) => {
    const type = this.props.fields[name].type
    let value = ''
    switch (type) {
      case 'checkbox':
        value = data.checked
        break
      default:
        if (data && data.value) {
          value = data.value
        } else {
          value = get(event, 'target.value', event)
        }
    }

    this.updateField(name, value)
  }

  updateField = (name, value) => {
    const controlValues = this.state.controlValues
    const controlValidationErrors = this.state.controlValidationErrors
    controlValues[name] = value
    if (this.state.shouldValidate) {
      this.validate()
    } else {
      controlValidationErrors[name] = ''
    }

    this.setState({ controlValues, controlValidationErrors })

    this.props.onUpdateField
      && this.props.onUpdateField(name, value)
  }

  validate = () => {
    return new Promise(resolve => {
      const controlValidationErrors = this.state.controlValidationErrors
      let isValid = true

      Object.keys(this.state.controlValues).forEach(controlName => {
        let isNotValid = false
        const validator = (this.props.fields[controlName] || {}).validator

        if (validator) {
          if (Array.isArray(validator)) {
            for (let item = 0; item < validator.length; item++) {
              isNotValid = validator[item](
                this.state.controlValues[controlName],
                this.state.controlValues
              )
              if (isNotValid) {
                break
              }
            }
          } else {
            isNotValid = validator(
              this.state.controlValues[controlName],
              this.state.controlValues
            )
          }
        }

        if (isNotValid) {
          isValid = false
          controlValidationErrors[controlName] = isNotValid
        } else {
          controlValidationErrors[controlName] = ''
        }
      })

      this.setState({ controlValidationErrors, shouldValidate: false })

      resolve(isValid)
    })
  }

  submit = event => {
    event.stopPropagation()
    event.preventDefault()
    this.setState({ shouldValidate: true })
    this.validate().then(isValid => {
      this.props.onSubmit(isValid ? this.state.controlValues : false)
    })
  }

  render() {
    if (
      typeof this.props.render === 'function'
        && this.props.render.prototype
        && !!this.props.render.prototype.isReactComponent
    ) {
      return (
        <this.props.render
          getControlProps={this.getControlProps}
          onSubmit={this.submit}
          updateField={this.updateField}
        />
      )
    } else {
      return this.props.render(
        this.getControlProps,
        this.submit,
        this.updateField
      )
    }
  }
}
