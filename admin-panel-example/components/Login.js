import React, { Component } from 'react'
import { request } from '../services/api'
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  Form,
  FormGroup,
  FormInput,
  Button
} from 'shards-react'
import { Link } from 'react-router-dom'
import BaseForm from './Form'
import { required, length, email } from './Form/validators';
import { fillWithErrors } from './Form/utils';
import App from '../config/App';
import pick from 'lodash/pick'
import RoleCode from '../enums/RoleCode';
import Logo from './Logo'

export default class Login extends Component {
  state = {
    password: '',
    fields: {
      email: {
        validator: [required, email]
      },
      password: {
        validator: [required, length(0, 255)]
      }
    },
    isLoading: false
  }

  componentDidMount() {
    if (localStorage.getItem('token')) {
      this.props.history.push({ pathname: '/' })
    }
  }

  onSubmit = data => {
    if (!data) {
      return
    }
    this.setState({
      isLoading: true
    })
    request('/auth', 'POST', {
      ...data,
      roleCodes: [RoleCode.ADMIN, RoleCode.MARKETING_ADMIN, RoleCode.MARKETER],
      companyId: new App().getCompany().id
    })
      .then(data => {
        this.setState({
          isLoading: false
        })
        localStorage.setItem('token', data.token)
        localStorage.setItem('refreshToken', data.refreshToken)

        this.props.history.push('/')
      })
      .catch(error => {
        this.setState({
          isLoading: false
        })
        if (!error.fields) {
          this.setState((state) => {
            return {
              fields: {
                ...state.fields,
                password: {
                  ...state.fields.password,
                  error: error.message
                }
              }
            }
          })
          return
        }
        this.setState((state) => {
          return {
            fields: fillWithErrors(error.fields, state.fields)
          }
        })
      })
  }

  onChange = (field, value) => {
    this.setState((state) => {
      return {
        fields: {
          ...state.fields,
          [field]: {
            ...state.fields[field],
            error: false
          }
        }
      }
    })
  }

  render() {
    return (
      <Container fluid className="main-content-container h-100 px-4">
        <Row noGutters className="h-100">
          <Col lg="12" md="12" className="auth-form mx-auto my-auto pt-3 pb-3">
            <Card>
              <CardBody>
                <Logo />
                <div className="auth-form__subtitle-wrapper">
                  <h4 className="auth-form__subtitle">Log in</h4>
                </div>
                <BaseForm
                  fields={this.state.fields}
                  onSubmit={this.onSubmit}
                  onUpdateField={this.onChange}
                  render={(getControlProps, submit) => {
                    const emailInput = getControlProps('email')
                    const passwordInput = getControlProps('password')
                    return (
                      <Form onSubmit={submit}>
                        <FormGroup>
                          <label
                            htmlFor="email"
                          >
                            Email
                          </label>

                          <FormInput
                            value={this.state.email}
                            {...pick(emailInput, ['value', 'onChange'])}
                            required
                            type="email"
                            id="email"
                            placeholder="Enter email"
                            invalid={!!emailInput.error}
                          />
                          {emailInput.error && (
                            <span className="input__error">{emailInput.error}</span>
                          )}

                        </FormGroup>
                        <FormGroup>
                          <label
                            htmlFor="password"
                          >
                            Password
                          </label>
                          <FormInput
                            value={this.state.password}
                            {...pick(passwordInput, ['value', 'onChange'])}
                            type="password"
                            id="password"
                            placeholder="Password"
                            autoComplete="current-password"
                            required
                            invalid={!!passwordInput.error}
                          />
                          {passwordInput.error && (
                            <span className="input__error">{passwordInput.error}</span>
                          )}
                        </FormGroup>
                        <Button
                          pill
                          theme="accent"
                          className="d-table mx-auto login-button"
                          type="submit"
                          disabled={this.state.isLoading}
                        >
                          Log in
                        </Button>
                      </Form>
                    )
                  }}
                />
                <div className="text-center auth-form__link">
                  <Link to="/forgot-password" className="simple-link">
                    Forgot your password?
                  </Link>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }
}
