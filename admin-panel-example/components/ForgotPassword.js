import React, { Component } from 'react'
import { request } from '../services/api'
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  Form,
  FormGroup,
  FormInput,
  Button
} from 'shards-react'
import { Link } from 'react-router-dom'
import BaseForm from './Form'
import { required, email } from './Form/validators'
import { fillWithErrors } from './Form/utils'
import Logo from './Logo'

export default class ForgotPassword extends Component {
  state = {
    isRequestSuccessful: false,
    fields: {
      email: {
        validator: [required, email]
      }
    },
    email: '',
    isLoading: false,
  }

  onSubmit = data => {
    if (!data) {
      return
    }
    this.setState({
      isLoading: true
    })
    request('/users/reset-password', 'GET', {
      email: data.email
    })
      .then(() => {
        this.setState({
          isRequestSuccessful: true,
          email: data.email,
          isLoading: false
        })
      })
      .catch(error => {
        this.setState({
          isLoading: false
        })
        if (!error.fields) {
          this.setState(state => {
            return {
              fields: {
                ...state.fields,
                email: {
                  ...state.fields.email,
                  error: error.message
                }
              }
            }
          })
          return
        }
        this.setState(state => {
          return {
            fields: fillWithErrors(error.fields, state.fields)
          }
        })
      })
  }

  onChange = (field, value) => {
    this.setState((state) => {
      return {
        fields: {
          ...state.fields,
          [field]: {
            ...state.fields[field],
            error: false
          }
        }
      }
    })
  }

  renderForm = (getControlProps, onSubmit) => {
    const emailProps = getControlProps('email')
    return (
      <Form onSubmit={onSubmit}>
        <FormGroup>
          <label htmlFor="email">
            Email
          </label>
          <FormInput
            {...emailProps}
            type="email"
            id="email"
            placeholder="Enter email"
            required
            invalid={!!emailProps.error}
          />
          {emailProps.error && (
            <span className="input__error">{emailProps.error}</span>
          )}
        </FormGroup>

        <Button
          pill
          theme="accent"
          className="d-table mx-auto login-button"
          type="submit"
          disabled={this.state.isLoading}
        >
          Next
        </Button>
      </Form>
    )
  }

  render() {
    return (
      <Container fluid className="main-content-container h-100 px-4">
        <Row noGutters className="h-100">
          <Col lg="12" md="12" className="auth-form mx-auto my-auto pt-3 pb-3">
            <Card>
              <CardBody>
                <Logo />
                {this.state.isRequestSuccessful ? (
                  <div className="auth-form__subtitle-wrapper">
                    <h4 className="auth-form__subtitle">
                      Check your email
                    </h4>
                    <p className="auth-form__text">
                      If {this.state.email} is associated with your
                    credentials, you should receive an email containing
                    instructions on how to create a new password.
                    </p>
                  </div>
                ) : (
                  <div>
                    <div className="auth-form__subtitle-wrapper">
                      <h4 className="auth-form__subtitle">
                        Forgot your password?
                      </h4>
                      <p className="auth-form__text">Enter your email to help us identify you.</p>
                    </div>
                    <BaseForm
                      fields={this.state.fields}
                      onSubmit={this.onSubmit}
                      render={this.renderForm}
                      onUpdateField={this.onChange}
                    />
                  </div>
                )}
                <div className="text-center auth-form__link">
                  <Link to="/login">
                    Return to Log in page
                  </Link>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }
}
