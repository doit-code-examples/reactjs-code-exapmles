import React from 'react'

export default function StarsRating(props) {
  const { rating } = props

  return <div className="stars-rating">
    <div className="stars-rating__foreground-wrapper">
      <div
        className="stars-rating__foreground"
        style={{
          width: `${rating / 5 * 100}%`
        }}>
        <span>★</span>
        <span>★</span>
        <span>★</span>
        <span>★</span>
        <span>★</span>
      </div>
    </div>
    <div className="stars-rating__background">
      <span>★</span>
      <span>★</span>
      <span>★</span>
      <span>★</span>
      <span>★</span>
    </div>
  </div>
}
