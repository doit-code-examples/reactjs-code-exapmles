import React from 'react';
import { Container, Button } from 'shards-react';
import { Link } from 'react-router-dom';

export default function NotFound(props) {
  return <Container fluid className="main-content-container px-4 pb-4">
    <div className="error">
      <div className="error__content">
        <h2>404</h2>
        <h3>Page not found</h3>
        <p>Page you are looking for doesn't exist</p>
        <Button pill tag={Link} to="/">Go to the Home Page</Button>
      </div>
    </div>
  </Container>
}
