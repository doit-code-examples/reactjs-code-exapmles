import React from 'react'
import {
  Button,
  Card,
  CardBody
} from 'shards-react'
import Logo from './Logo'

export default function SuccessMessage(props) {
  return <Card className="mb-3 success-message">
    <CardBody>
      <Logo />
      <h5 className="success-message__subtitle">
        Success
      </h5>
      <p className="success-message__message">{props.message}</p>
      <Button
        pill
        theme="accent"
        className="d-table mx-auto"
        type="button"
        onClick={props.onButtonClick}
      >
        Got It
      </Button>
    </CardBody>
  </Card>
}
