import React from 'react';
import { Container, Button } from 'shards-react';
import { Link } from 'react-router-dom';
import NotFound from './NotFound';
import NetworkError from '../errors/NetworkError';

const descriptionsMap = {
  403: 'You have no access to view this page'
}

export default function PageError(props) {
  const { error } = props
  if (error.status === 404) {
    return <NotFound />
  }
  return <Container fluid className="main-content-container px-4 pb-4">
    <div className="error">
      <div className="error__content">
        {error.status && <h2>{error.status}</h2>}
        <h3>{error.message}</h3>
        <p>{descriptionsMap[error.status] || null}</p>
        {error instanceof NetworkError
          ? <Button pill tag="a" href={window.location.href}>Reload</Button>
          : <Button pill tag={Link} to="/">Go to the Home Page</Button>
        }
      </div>
    </div>
  </Container>
}
