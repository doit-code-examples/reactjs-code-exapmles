import React, { Component } from 'react';
import { Button, Modal, ModalBody, ModalHeader } from 'shards-react';

export const Levels = {
  CRITICAL: 1
}

export default class ConfirmationModal extends Component {
  state = {
    open: true
  }

  toggle = () => {
    this.setState({
      open: !this.state.open
    })
    if (this.state.open) {
      if (typeof this.props.onClose === 'function') {
        this.props.onClose()
      } else if (typeof this.props.onCancel === 'function') {
        this.props.onCancel()
      }
    }
  }

  render() {
    const { open } = this.state
    const { level } = this.props

    return <Modal centered size="sm" className="modal--confirmation" open={open} toggle={this.toggle}>
      <div className="modal__close">
        <i className="material-icons" onClick={this.toggle}>
          close
        </i>
      </div>
      <ModalHeader
        className={'modal__header' + (level === Levels.CRITICAL
          ? ' modal__header--critical'
          : ' modal__header--confirmation')
        }
      >
        Confirmation
      </ModalHeader>
      <ModalBody className="text-center">
        <p>{this.props.message}</p>
        <div className="modal__buttons">
          <Button
            theme="light"
            pill
            className="mr-4"
            type="button"
            onClick={this.props.onCancel}
          >
            Cancel
          </Button>
          <Button
            theme={level === Levels.CRITICAL ? 'danger' : 'accent'}
            pill
            type="button"
            onClick={this.props.onConfirm}
          >
            {this.props.confirmButtonText || 'Confirm'}
          </Button>
        </div>
      </ModalBody>
    </Modal>
  }
}
