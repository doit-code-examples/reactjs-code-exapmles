import 'core-js/es/map';
import 'core-js/es/set';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import App from './App';
import auth from './reducers/auth';
import company from './reducers/company';
import layout from './reducers/layout';

const store = createStore(
  combineReducers({
    auth,
    layout,
    company
  }),
  {
    company: window.APP_STATE.company,
  },
  applyMiddleware(thunkMiddleware)
);

render(<Provider store={store}><App /></Provider>, document.getElementById('app'))
