import React, { Component } from 'react';
import { Col, Container, Row } from 'shards-react';
import { authorize } from '../actions/auth';
import { hideConfirmation } from '../actions/layout';
import { get } from '../api/user';
import ConfirmationModal from '../components/ConfirmationModal';
import MainNavbar from '../components/MainNavbar/MainNavbar';
import MainSidebar from '../components/MainSidebar/MainSidebar';

export default class Dashboard extends Component {
  componentDidMount() {
    if (!localStorage.getItem('token')) {
      this.props.history.push({pathname: '/login'})
      return
    }

    get('me', {
      include: 'userRoles.[role]'
    })
      .then(({user}) => {
        this.props.dispatch(authorize(user))
      })
      .catch(error => {
        if (error.status === 401) {
          localStorage.removeItem('token')
          localStorage.removeItem('refreshToken')
          this.props.history.push('/login')
        }
      })
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.location.pathname !== prevProps.location.pathname
      && !localStorage.getItem('token')
    ) {
      this.props.history.push({ pathname: '/login' })
    }
  }

  render() {
    if (!this.props.auth.user) {
      return null
    }

    return (
      <Container fluid>
        <Row>
          <MainSidebar
            history={this.props.history}
            layout={this.props.layout}
            dispatch={this.props.dispatch}
          />
          <Col
            className="main-content p-0"
            lg={{ size: 10, offset: 2 }}
            md={{ size: 9, offset: 3 }}
            sm="12"
            tag="main"
          >
            <MainNavbar company={this.props.company} />
            {this.props.children}
          </Col>
        </Row>
        {this.props.layout.showConfirmationModal
          && <ConfirmationModal
            message="You are going to leave the page without saving the chages.
            Are you sure you want to leave the page?"
            confirmButtonText="Leave"
            onConfirm={this.props.layout.onLeaveConfirm}
            onCancel={() => { this.props.dispatch(hideConfirmation()) }}
          />
        }
      </Container>
    )
  }
}
