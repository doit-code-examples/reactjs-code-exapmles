import { Component } from 'react'

export default class Blank extends Component {
  constructor(props) {
    super(props)
    if (!localStorage.getItem('token')) {
      this.props.history.push({ pathname: '/login' })
    }
  }

  render() {
    return this.props.children
  }
}
