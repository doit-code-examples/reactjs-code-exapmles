import {Types} from '../reducers/auth';

export function authorize(user) {
    return {
        type: Types.AUTHORIZE,
        user
    }
}
