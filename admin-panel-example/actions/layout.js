import { Types } from '../reducers/layout';

function leavePageListener(event) {
  event.returnValue = 'o/';
}

export function showConfirmation(onLeaveConfirm) {
  return {
    type: Types.TOGGLE_SHOWING_CONFIRMATION,
    data: {
      onLeaveConfirm
    }
  }
}

export function setActionRequiredBeforeLeave() {
  window.addEventListener('beforeunload', leavePageListener);
  return {
    type: Types.SET_ACTION_REQUIRED_BEFORE_LEAVE,
  }
}

export function unsetActionRequiredBeforeLeave() {
  window.removeEventListener('beforeunload', leavePageListener);
  return {
    type: Types.UNSET_ACTION_REQUIRED_BEFORE_LEAVE,
  }
}

export function hideConfirmation() {
  return {
    type: Types.TOGGLE_SHOWING_CONFIRMATION
  }
}
