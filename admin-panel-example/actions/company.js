import {Types} from '../reducers/company';

export function companyAdd(company) {
    return {
        type: Types.COMPANY,
        company
    }
}
